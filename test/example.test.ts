import App from "../src/application/App";
import {IUser, Role} from "../src/domain/entity/auth/IUser";
import Connection from "../src/infrastructure/orm/Connection";

import * as mongoose from "mongoose";
import dotenv from "dotenv";
import supertest from "supertest";

let connection: mongoose.Connection;

beforeAll(async () => {
    dotenv.config();
    connection = await Connection();
});

afterAll(async (done) => {
    await connection.close();
    done();
});

describe('Check Start server config', () => {
    test("server response [200] OK", (done) => {

        const validUser : Partial<IUser> = {
            active: false,
            graduate: false,
            roll: Role.STUDENT,
            name: "Christopher Gerardy",
            lastname: "Andrade Lazcano",
            email: "christophergerardy778@gmail.com",
            password: "GEO699"
        };

        supertest(App)
            .post("/api/auth/register")
            .send(validUser)
            .expect(200)
            .expect({
                ok: true,
                data: "works",
            }, done);
    });
})