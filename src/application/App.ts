import router from "../interface/routes/router";

import morgan from "morgan";
import express, { Application, json, urlencoded } from "express";
import cors from "cors";
import path from "path";
import FirebaseService from "./services/FirebaseService";
import Notice from "../infrastructure/orm/Notice";
import {ModelPopulateOptions} from "mongoose";
import {Email} from "./services/EmailService";

const app: Application = express();
const publicPath = path.resolve(__dirname, "../../public");

app.use(json());
app.use(morgan("dev"));
app.use(urlencoded({ extended: false }));
app.use(cors());

app.set("port", 3000);

app.use("/api", router);

app.use(express.static(publicPath));

app.use("/notify", async (req, res) => {
    /*const result = await FirebaseService.getService()
        .sendNotification(["eRY4gatW6Q8:APA91bEyT-XLO9RTXIRLirGbZWAT8f_iNImPIfasKhSInm7fJm8Niau3e3dt0QbD-UndJpcczkKx6cjzRJ_O9vCzTiF3KSuz4FquZWM4Z6HoKmxp-mww5eibsUX_fOclD7und5n-M2CX"],
            "Webin creo una nueva notificacion",
            "Hola webin por favor atiende esta notificacion es genial :D",
            "https://images.pexels.com/photos/1005644/pexels-photo-1005644.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940");*/

    console.log(process.env.EMAIL_FROM);
    console.log(process.env.EMAIL_PASSWORD);

    const email = Email.EmailBuilder()
        .to("angel210521@gmail.com")
        .layout("test")
        .subject("Prueba de envio de emails del tesco sic")
        .build();

    const result = await Email.sendEmail(email);

    return res.send({
        ok: true,
        data: result
    })
});

export default app;