import App from "./App";
import HttpServer from "./HttpServer";
import createConnection from "../infrastructure/orm/Connection";
import WebSocket from "../interface/websocket/Socket";
import createPeerServer from "../interface/peer/Peer";

import { config } from "dotenv";
import * as os from "os";
import clear from "clear";

function main() {
    clear();
    config();
    WebSocket(HttpServer);

    createConnection().then(() => {
        App.use("/peerjs", createPeerServer(HttpServer));

        HttpServer.listen(App.get("port"), () => {
            console.log("SO: " +  os.platform());
            console.log("ARCH" + os.arch());
            console.log("Type" + os.type());
        });
    });
}

main();