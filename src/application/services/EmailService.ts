import path from "path";
import {createTransport} from "nodemailer";
import hbs from "nodemailer-express-handlebars";

interface IEmail {
    from: string,
    cc?: string,
    to: string,
    context?: any
    subject: string,
    template: string,
}

class EmailBuilder {

    private _to!: string;
    private _subject!: string;
    private _withCopy!: string;
    private _layout!: string;
    private _context!: any;

    public to(value: string) {
        this._to = value;
        return this;
    }

    public subject(value: string) {
        this._subject = value;
        return this;
    }

    public withCopy(value: string) {
        this._withCopy = value;
        return this;
    }

    public layout(value: string) {
        this._layout = value;
        return this;
    }

    public context(value: any) {
        this._context = value;
        return this;
    }

    public build() : IEmail {
        return {
            to: this._to,
            cc: this._withCopy,
            from: process.env.EMAIL_FROM!,
            subject: this._subject,
            template: this._layout,
            context: this._context
        }
    }
}

export class Email {

    public static EmailBuilder() {
        return new EmailBuilder();
    }

    public static sendEmail(emailBuild: IEmail) {

        const transport = createTransport({
            service: process.env.EMAIL_SERVICE,
            auth: {
                user: process.env.EMAIL_FROM,
                pass: process.env.EMAIL_PASSWORD
            },
            debug: process.env.NODE_ENV === "DEV"
        });

        transport.use('compile', hbs({
            extName: '.hbs',
            viewPath: path.resolve(__dirname, '../../../email'),

            viewEngine: {
                extname: '.hbs',
                layoutsDir: path.resolve(__dirname, '../../../email/layouts'),
                defaultLayout: 'default'
            }
        }));

        return transport.sendMail(emailBuild)
    }
}