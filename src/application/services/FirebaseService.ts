import * as admin from "firebase-admin";
import {NotificationBuild, NotificationType} from "../../domain/entity/server/NotificationFirebase";

const credentialApp = require("../../infrastructure/config/tesco_firebase.json");

export default class FirebaseService {

    private readonly app: admin.app.App;
    private static instance: FirebaseService;

    public static getService() {
        if (FirebaseService.instance === undefined) FirebaseService.instance = new FirebaseService();
        return FirebaseService.instance;
    }

    constructor() {
        this.app = admin.initializeApp({
            credential: admin.credential.cert(credentialApp)
        });
    }

    private shortBody(text: string) : string{
        if (text.length > 80) return `${text.slice(0, 80)}...`;
        else return  text;
    }

    private shortTitle(title: string) {
        if (title.length > 40) return `${title.slice(0, 40)}...`;
        else return title;
    }

    private setLinkByNotificationType(type: NotificationType, source_id: string) : string{
        switch (type) {
            case NotificationType.NoticeNotification:
                return `${process.env.CLIENT_URL}/anuncio/${source_id}`;
            case NotificationType.PublicationNotification:
                return `${process.env.CLIENT_URL}/publicacion/${source_id}`;
            default:
                return process.env.CLIENT_URL!;
        }
    }

    public sendNotificationByType(data: NotificationBuild) {
        return this.app.messaging().sendMulticast({
            tokens: data.tokens,

            notification: {
                title: this.shortTitle(data.title),
                body: this.shortBody(data.body),
                imageUrl: data.imageUrl
            },

            webpush: {
                fcmOptions: {
                    link: this.setLinkByNotificationType(data.type, data.source_id)
                }
            }
        });
    }
}