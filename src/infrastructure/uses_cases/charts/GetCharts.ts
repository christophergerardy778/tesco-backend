import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {GetChartsPort} from "./port/GetChartsPort";
import {IChartRepository} from "../../../domain/repository/charts/IChartRepository";
import {ChartRepository} from "../../../domain/repository/charts/ChartRepository";
import {UserDocument} from "../../orm/User";
import {ProfileDocument} from "../../orm/Profile";

export class GetCharts implements IBaseUseCase<GetChartsPort, any> {

    private chartRepository!: IChartRepository;

    constructor() {
        this.chartRepository = new ChartRepository();
    }

    async execute(port: GetChartsPort): Promise<any> {
        const profiles: UserDocument["_id"] = [];
        const profileQuery = await this.chartRepository.getProfileIds();

        profileQuery.forEach((item: Partial<UserDocument>) => {
            if (item.profile !== undefined) profiles.push(item.profile);
        });

        const usersWithJob = await this.chartRepository.getGraduateAlumnus(profiles)

        const graduates_job_chart = {
            total_graduates: profiles.length,
            has_job: usersWithJob.length,
            without_job: profiles.length - usersWithJob.length
        };

        //

        const alumnusQuery = await this.chartRepository.getAlumnus();
        const alumnus: ProfileDocument["_id"][] = [];

        alumnusQuery.forEach((item: Partial<UserDocument>) => {
            if (item.profile !== undefined) alumnus.push(item.profile);
        });

        const alumnusWithJob = await this.chartRepository.getGraduateAlumnus(alumnus)

        const alumnus_job_chart = {
            total_alumnus: alumnus.length,
            has_job: alumnusWithJob.length,
            without_job: alumnus.length - alumnusWithJob.length
        }

        //
        const graduatesWithExperienceQuery = await this.chartRepository.getGraduatesWithExperience();

        const graduates_with_experience = {
            total_graduates: profiles.length,
            with_experience: graduatesWithExperienceQuery.length,
            without_experience: profiles.length - graduatesWithExperienceQuery.length
        }

        // con perfil de linkedin
        const alumnusWithLinkedinQuery = await this.chartRepository.getGraduatesWithLinkedinProfile()

        const alumnus_with_linkedin = {
            total_alumnus: profiles.length,
            with_linkedin: alumnusWithLinkedinQuery.length,
            without_linkedin: profiles.length - alumnusWithLinkedinQuery.length
        }

        // lenguajes mas populares(tecnologias)

        const popular_technologies = await this.chartRepository.getPopularSkills();

        return {
            alumnus_job_chart,
            graduates_job_chart,
            alumnus_with_linkedin,
            graduates_with_experience,
            popular_technologies
        };
    }
}