export interface UpdateSkillPort {
    skill_id: string;
    color: string;
    name: string;
}