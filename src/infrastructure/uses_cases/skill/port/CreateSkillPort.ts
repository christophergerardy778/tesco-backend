export interface CreateSkillPort {
    name: string;
    color: string;
}