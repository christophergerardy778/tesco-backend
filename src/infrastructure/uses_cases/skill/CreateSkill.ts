import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {CreateSkillPort} from "./port/CreateSkillPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {SkillDocument} from "../../orm/Skill";
import {ISkillRepository} from "../../../domain/repository/skill/ISkillRepository";
import SkillRepository from "../../../domain/repository/skill/SkillRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export default class createSkill implements IBaseUseCase<CreateSkillPort, HttpResponse<SkillDocument>> {

    private readonly repository!: ISkillRepository;

    constructor() {
        this.repository = new SkillRepository();
    }

    async execute(port: CreateSkillPort): Promise<HttpResponse<SkillDocument>> {
        try {
            const skill = await this.repository.createSkill(port.name, port.color);

            return {
                ok: true,
                data: skill,
                message: OPERATION_SUCCESS.message
            }

        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }

}