import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {ISkillRepository} from "../../../domain/repository/skill/ISkillRepository";
import SkillRepository from "../../../domain/repository/skill/SkillRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";
import {UpdateSkillPort} from "./port/UpdateSkillPort";

export default class UpdateSkill implements IBaseUseCase<UpdateSkillPort, HttpResponse<string>> {

    private readonly repository!: ISkillRepository;

    constructor() {
        this.repository = new SkillRepository();
    }

    async execute(port: UpdateSkillPort): Promise<HttpResponse<string>> {
        try {
            await this.repository.updateSkill(port.skill_id, port.name, port.color);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }

        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}