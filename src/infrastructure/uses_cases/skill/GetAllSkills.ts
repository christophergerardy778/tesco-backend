import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {GetSkillsPort} from "./port/GetSkillsPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {SkillDocument} from "../../orm/Skill";
import {ISkillRepository} from "../../../domain/repository/skill/ISkillRepository";
import SkillRepository from "../../../domain/repository/skill/SkillRepository";
import {SERVER_ERROR} from "../../../shared/util/Constants";

export default class GetAllSkills implements IBaseUseCase<GetSkillsPort, HttpResponse<SkillDocument[]>> {

    private readonly repository!: ISkillRepository;

    constructor() {
        this.repository = new SkillRepository();
    }

    async execute(port: GetSkillsPort): Promise<HttpResponse<SkillDocument[]>> {
        try {
            const result = await this.repository.getAllSkills();

            return {
                ok: true,
                data: result
            }

        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message,
            }
        }
    }
}