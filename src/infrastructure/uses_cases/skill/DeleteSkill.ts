import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {DeleteSkillPort} from "./port/DeleteSkillPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {ISkillRepository} from "../../../domain/repository/skill/ISkillRepository";
import SkillRepository from "../../../domain/repository/skill/SkillRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export default class DeleteSkill implements IBaseUseCase<DeleteSkillPort, HttpResponse<string>> {

    private readonly repository!: ISkillRepository;

    constructor() {
        this.repository = new SkillRepository();
    }

    async execute(port: DeleteSkillPort): Promise<HttpResponse<string>> {
        try {
            await this.repository.deleteSkill(port.skill_id);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }

        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}