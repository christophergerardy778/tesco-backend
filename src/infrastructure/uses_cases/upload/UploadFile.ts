import {
    FILE_PDF,
    IMAGE_TYPES,
    SERVER_ERROR,
    VIDEO_TYPES,
    WORD_CLASSIC_DOC,
    WORD_DOC
} from "../../../shared/util/Constants";
import {UploadFilePort} from "./port/UploadFilePort";
import {IFileTypes} from "../../../domain/entity/files/IFile";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {IUploadRepository} from "../../../domain/repository/upload/IUploadRepository";
import {FileDocument} from "../../orm/File";

import path from "path";

export default class UploadFile implements IBaseUseCase<UploadFilePort, HttpResponse<FileDocument[]>> {

    private readonly repository: IUploadRepository;

    constructor(repository: IUploadRepository) {
        this.repository = repository;
    }

    async execute(port: UploadFilePort): Promise<HttpResponse<FileDocument[]>> {
        try {
            const result: FileDocument[] = [];

            for (const file of port.files) {

                let fileDocument: FileDocument;
                const fileExt = path.extname(file.filename);

                if (IMAGE_TYPES.includes(fileExt)) {

                    fileDocument = await this.repository.createFile(file.filename, IFileTypes.IMAGE);
                    result.push(fileDocument);

                } else if (VIDEO_TYPES.includes(fileExt)) {

                    fileDocument = await this.repository.createFile(file.filename, IFileTypes.VIDEO);
                    result.push(fileDocument);

                } else if (WORD_DOC === fileExt || WORD_CLASSIC_DOC === fileExt) {

                    fileDocument = await this.repository.createFile(file.filename, IFileTypes.DOC);
                    result.push(fileDocument);

                } else if (FILE_PDF === fileExt) {

                    fileDocument = await this.repository.createFile(file.filename, IFileTypes.PDF);
                    result.push(fileDocument);

                }
            }

            return {
                ok: true,
                data: result,
            }

        } catch (e) {

            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}