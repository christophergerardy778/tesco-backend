import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {IUploadRepository} from "../../../domain/repository/upload/IUploadRepository";
import {DeleteFilePort} from "./port/DeleteFilePort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {BAD_REQUEST} from "../../../shared/util/Constants";

import * as fs from "fs";
import path from "path";

export default class DeleteFile implements IBaseUseCase<DeleteFilePort, HttpResponse<string>> {

    private readonly repository: IUploadRepository;

    constructor(repository: IUploadRepository) {
        this.repository = repository;
    }

    async execute(port: DeleteFilePort): Promise<HttpResponse<string>> {
        try {
            const fileNameArray = [];
            const fileDocuments = await this.repository.findMany(port.fileIdArray);

            for (const doc of fileDocuments) {
                fileNameArray.push(doc.name);
            }

            for (const fileName of fileNameArray) {
                fs.unlinkSync(path.resolve(__dirname, `../../../../public/storage/${fileName}`));
            }

            await this.repository.deleteMany(port.fileIdArray);

            return {
                ok: true,
                data: "SUCCESS"
            }
        } catch (e) {
            console.log(e);

            return {
                ok: true,
                error: BAD_REQUEST.message
            }
        }
    }
}