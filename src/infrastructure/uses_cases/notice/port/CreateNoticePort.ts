import {INotice} from "../../../../domain/entity/notice/INotice";
import {ILayout} from "../../../../domain/entity/layout/ILayout";

export interface CreateNoticePort {
    user_id: string;
    notice: INotice,
    layouts: ILayout[]
}