import {CreateNoticePort} from "./port/CreateNoticePort";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {INoticeRepository} from "../../../domain/repository/notice/INoticeRepository";
import {ILayoutRepository} from "../../../domain/repository/layout/ILayoutRepository";
import FirebaseService from "../../../application/services/FirebaseService";
import {NotificationType} from "../../../domain/entity/server/NotificationFirebase";

export default class CreateNotice implements IBaseUseCase<CreateNoticePort, HttpResponse<string>> {

    private readonly repository: INoticeRepository;
    private readonly layoutRepository: ILayoutRepository;

    constructor(repository: INoticeRepository, layoutRepository: ILayoutRepository) {
        this.repository = repository;
        this.layoutRepository = layoutRepository;
    }

    async execute(port: CreateNoticePort): Promise<HttpResponse<string>> {
        const layoutIds = []
        const { title, cover, description } = port.notice;
        const layouts = await this.layoutRepository.createLayouts(port.layouts);

        for (const layout of layouts) {
            layoutIds.push(layout._id);
        }

        const notice = await this.repository.createNotice(title, description, port.user_id, layoutIds, cover);

        await FirebaseService.getService().sendNotificationByType({
            tokens: ["eRY4gatW6Q8:APA91bEyT-XLO9RTXIRLirGbZWAT8f_iNImPIfasKhSInm7fJm8Niau3e3dt0QbD-UndJpcczkKx6cjzRJ_O9vCzTiF3KSuz4FquZWM4Z6HoKmxp-mww5eibsUX_fOclD7und5n-M2CX"],
            title: notice.title,
            type: NotificationType.NoticeNotification,
            source_id: notice._id,
            body: notice.description,
            imageUrl: notice.cover
        });

        return {
            ok: true,
            data: "SUCCESS"
        };
    }

}