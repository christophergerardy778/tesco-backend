import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {GetNoticeByIdPort} from "./port/GetNoticeByIdPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {NoticeDocument} from "../../orm/Notice";
import {NOT_FOUND, SERVER_ERROR} from "../../../shared/util/Constants";
import {INoticeRepository} from "../../../domain/repository/notice/INoticeRepository";

export default class GetNoticeById implements IBaseUseCase<GetNoticeByIdPort, HttpResponse<NoticeDocument>> {

    private readonly repository!: INoticeRepository;

    constructor(repository: INoticeRepository) {
        this.repository = repository;
    }

    async execute(port: GetNoticeByIdPort): Promise<HttpResponse<NoticeDocument>> {
        try {
            const result = await this.repository.getNoticeById(port.notice_id);

            if (result === null) {
                return {
                    ok: false,
                    error: NOT_FOUND.message
                };
            }

            return {
                ok: true,
                data: result
            };
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}