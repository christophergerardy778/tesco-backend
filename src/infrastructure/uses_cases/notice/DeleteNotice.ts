import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {DeleteNoticePort} from "./port/DeleteNoticePort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {INoticeRepository} from "../../../domain/repository/notice/INoticeRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export default class DeleteNotice implements IBaseUseCase<DeleteNoticePort, HttpResponse<string>> {

    private readonly repository!: INoticeRepository;

    constructor(repository: INoticeRepository) {
        this.repository = repository;
    }

    async execute(port: DeleteNoticePort): Promise<HttpResponse<string>> {
        try {
            await this.repository.deleteNoticeById(port.notice_id);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message,
            };
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}