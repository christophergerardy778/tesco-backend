import {NoticeDocument} from "../../orm/Notice";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {INoticeRepository} from "../../../domain/repository/notice/INoticeRepository";
import {BAD_REQUEST} from "../../../shared/util/Constants";
import {GetPaginateNotices} from "./port/GetPaginateNotices";

export default class GetNotices implements IBaseUseCase<GetPaginateNotices, HttpResponse<NoticeDocument[]>> {

    private readonly repository: INoticeRepository;

    constructor(repository: INoticeRepository) {
        this.repository = repository;
    }

    async execute(port: GetPaginateNotices): Promise<HttpResponse<NoticeDocument[]>> {
        try {
            const notices = await this.repository.getPaginateNotices(port.page);

            return {
                ok: true,
                data: notices
            };

        } catch (e) {
            console.log(e);

            return {
                ok: true,
                error: BAD_REQUEST.message
            };
        }
    }
}