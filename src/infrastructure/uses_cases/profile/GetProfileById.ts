import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {GetProfilePort} from "./port/GetProfilePort";
import ProfileRepository from "../../../domain/repository/profile/ProfileRepository";
import {NOT_FOUND, SERVER_ERROR} from "../../../shared/util/Constants";
import {UserDocument} from "../../orm/User";

export default class GetProfileById implements  IBaseUseCase<GetProfilePort, HttpResponse<UserDocument>> {

    private readonly repository!: ProfileRepository;

    constructor() {
        this.repository = new ProfileRepository();
    }

    async execute(port: GetProfilePort): Promise<HttpResponse<UserDocument>> {
        try {
            const user = await this.repository.getProfileById({
                user_id: port.user_id
            });

            if (!user) {
                return {
                    ok: false,
                    error: NOT_FOUND.message
                }
            }

            return {
                ok: true,
                data: user
            }
        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}