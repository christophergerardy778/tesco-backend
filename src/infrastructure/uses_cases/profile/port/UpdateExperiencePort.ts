import {ExperienceDocument} from "../../../orm/Experience";

export interface UpdateExperiencePort {
    id: ExperienceDocument["_id"];
    experience: Partial<ExperienceDocument>;
}