import {ProfileDocument} from "../../../orm/Profile";
import {IExperience} from "../../../../domain/entity/experience/IExperience";

export interface AddExperiencePort {
    id: ProfileDocument["_id"];
    experience: IExperience;
}