import {ProfileDocument} from "../../../orm/Profile";

export interface UpdateProfilePhotoPort {
    id: ProfileDocument["_id"];
    photo: Express.Multer.File;
}