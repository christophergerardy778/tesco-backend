import {ProfileDocument} from "../../../orm/Profile";
import {SkillDocument} from "../../../orm/Skill";

export interface UpdateSkillsPort {
    id: ProfileDocument["_id"];
    skills: SkillDocument["_id"][];
}