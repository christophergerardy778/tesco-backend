import {ExperienceDocument} from "../../../orm/Experience";
import {ProfileDocument} from "../../../orm/Profile";

export interface DeleteExperiencePort {
    id: ExperienceDocument["_id"];
    profile_id: ProfileDocument["_id"];
}