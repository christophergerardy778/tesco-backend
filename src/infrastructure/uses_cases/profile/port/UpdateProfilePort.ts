import {ProfileDocument} from "../../../orm/Profile";
import {IProfile} from "../../../../domain/entity/profile/IProfile";

export interface UpdateProfilePort {
    id: ProfileDocument["_id"];
    profile: Partial<IProfile>;
}