import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {UpdateSkillsPort} from "./port/UpdateSkillsPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IProfileRepository} from "../../../domain/repository/profile/IProfileRepository";
import ProfileRepository from "../../../domain/repository/profile/ProfileRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export class UpdateSkills implements IBaseUseCase<UpdateSkillsPort, HttpResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        this.repository = new ProfileRepository();
    }

    async execute(port: UpdateSkillsPort): Promise<HttpResponse<string>> {
        try {
            await this.repository.updateSkills(port);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message,
            }
        }
    }
}