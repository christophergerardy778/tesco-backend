import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {UpdateProfilePort} from "./port/UpdateProfilePort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IProfileRepository} from "../../../domain/repository/profile/IProfileRepository";
import ProfileRepository from "../../../domain/repository/profile/ProfileRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export class UpdateProfile implements IBaseUseCase<UpdateProfilePort, HttpResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        this.repository = new ProfileRepository();
    }

    async execute(port: UpdateProfilePort): Promise<HttpResponse<string>> {
        try {
            await this.repository.updateProfile(port);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}