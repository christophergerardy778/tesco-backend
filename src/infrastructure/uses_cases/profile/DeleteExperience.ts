import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {DeleteExperiencePort} from "./port/DeleteExperiencePort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IProfileRepository} from "../../../domain/repository/profile/IProfileRepository";
import ProfileRepository from "../../../domain/repository/profile/ProfileRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export class DeleteExperience implements IBaseUseCase<DeleteExperiencePort, HttpResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        this.repository = new ProfileRepository();
    }

    async execute(port: DeleteExperiencePort): Promise<HttpResponse<string>> {
        try {
            await this.repository.deleteExperience(port);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}