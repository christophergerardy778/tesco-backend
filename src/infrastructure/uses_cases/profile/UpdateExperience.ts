import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IProfileRepository} from "../../../domain/repository/profile/IProfileRepository";
import ProfileRepository from "../../../domain/repository/profile/ProfileRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";
import {UpdateExperiencePort} from "./port/UpdateExperiencePort";

export class UpdateExperience implements IBaseUseCase<UpdateExperiencePort, HttpResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        this.repository = new ProfileRepository();
    }

    async execute(port: UpdateExperiencePort): Promise<HttpResponse<string>> {
        try {
            await this.repository.updateExperience(port);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}