import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {AddExperiencePort} from "./port/AddExperiencePort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IProfileRepository} from "../../../domain/repository/profile/IProfileRepository";
import ProfileRepository from "../../../domain/repository/profile/ProfileRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export class AddExperience implements IBaseUseCase<AddExperiencePort, HttpResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        this.repository = new ProfileRepository();
    }

    async execute(port: AddExperiencePort): Promise<HttpResponse<string>> {
        try {
            await this.repository.addExperience(port);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}