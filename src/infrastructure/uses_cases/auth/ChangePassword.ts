import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {BAD_REQUEST, NOT_FOUND, OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";
import {ChangePasswordPort} from "./port/ChangePasswordPort";
import {decodeToken, isValidToken} from "../../../shared/util/WebToken";
import {generateHash} from "../../../shared/util/HashString";

export default class ChangePassword implements IBaseUseCase<ChangePasswordPort, HttpResponse<string>> {

    private readonly repository!: IAuthRepository;

    constructor(repository: IAuthRepository) {
        this.repository = repository;
    }

    async execute(port: ChangePasswordPort): Promise<HttpResponse<string>> {
        try {
            const tokenValid = isValidToken(port.token);

            if (!tokenValid) {
                return {
                    ok: false,
                    error: BAD_REQUEST.message
                }
            }

            const payload = decodeToken(port.token) as any;
            const userExists = await this.repository.getUserByEmail(payload.email);

            if (userExists === null) {
                return {
                    ok: false,
                    error: NOT_FOUND.message
                }
            }

            const hash = await generateHash(port.password);
            await this.repository.updatePassword(payload.email, hash);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}