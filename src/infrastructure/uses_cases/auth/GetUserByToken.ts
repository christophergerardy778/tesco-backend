import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {GetUserByTokenPort} from "./port/GetUserByTokenPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {UserDocument} from "../../orm/User";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import {decodeToken} from "../../../shared/util/WebToken";
import {BAD_REQUEST, NOT_FOUND, SERVER_ERROR} from "../../../shared/util/Constants";

export default class GetUserByToken implements IBaseUseCase<GetUserByTokenPort, HttpResponse<UserDocument>> {

    private readonly repository!: IAuthRepository;

    constructor(repository: IAuthRepository) {
        this.repository = repository;
    }

    async execute(port: GetUserByTokenPort): Promise<HttpResponse<UserDocument>> {
        try {
            const decodeData: any = await decodeToken(port.token);
            const userClean = await this.repository.getCleanUser(decodeData.email);

            if (decodeData.email === undefined) {
                return {
                    ok: false,
                    error: BAD_REQUEST.message
                }
            }

            if (userClean == null) {
                return {
                    ok: false,
                    error: NOT_FOUND.message
                }
            }

            return {
                ok: true,
                data: userClean
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message,
            }
        }
    }
}