import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import {ResetPasswordPort} from "./port/ResetPasswordPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {NOT_FOUND, OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";
import {Email} from "../../../application/services/EmailService";
import {generateTokenWithLimit} from "../../../shared/util/WebToken";

export default class ResetPassword implements IBaseUseCase<ResetPasswordPort, HttpResponse<string>> {

    private readonly repository!: IAuthRepository;

    constructor(repository: IAuthRepository) {
        this.repository = repository;
    }

    async execute(port: ResetPasswordPort): Promise<HttpResponse<string>> {
        try {
            const userExists = await this.repository.getUserByEmail(port.email);

            if (userExists === null) {
                return {
                    ok: false,
                    error: NOT_FOUND.message
                }
            }

            const fullName = `${userExists.name} ${userExists.lastname}`;

            const tokenLimit = await generateTokenWithLimit({
                name: fullName,
                email: userExists.email,
            }, '5m');

            const email = Email.EmailBuilder()
                .to(userExists.email)
                .subject('Restablecer contraseña')
                .layout('reset_password')
                .context({
                    name: fullName,
                    email: userExists.name,
                    token: tokenLimit,
                })
                .build();

            await Email.sendEmail(email);

            return {
                ok: true,
                message: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message,
            }
        }
    }
}