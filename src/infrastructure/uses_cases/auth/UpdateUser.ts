import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {UpdateUserPort} from "./port/UpdateUserPort";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import AuthRepository from "../../../domain/repository/auth/AuthRepository";

export class UpdateUser implements IBaseUseCase<UpdateUserPort, HttpResponse<string>> {

    private readonly repository!: IAuthRepository;

    constructor() {
        this.repository = new AuthRepository();
    }

    async execute(port: UpdateUserPort): Promise<HttpResponse<string>> {
        try {
            await this.repository.updateUser(port.id, port.user);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}