import {RegisterUserPort} from "./port/RegisterUserPort";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import {IUser} from "../../../domain/entity/auth/IUser";
import {generateToken} from "../../../shared/util/WebToken";
import {IProfileRepository} from "../../../domain/repository/profile/IProfileRepository";

export default class RegisterUser implements IBaseUseCase<RegisterUserPort, HttpResponse<IUser>>{

    private authRepository: IAuthRepository;
    private profileRepository: IProfileRepository;

    constructor(authRepository: IAuthRepository, profileRepository: IProfileRepository) {
        this.authRepository = authRepository;
        this.profileRepository = profileRepository;
    }

    async execute(port: RegisterUserPort): Promise<HttpResponse<IUser>> {
        try {

            const userExist = await this.authRepository.getOneUser({
                email: port.user.email
            });

            if (userExist !== null) {
                return {
                    ok: false,
                    error: 'EMAIL ALREADY ON USE',
                }
            }

            const profile = await this.profileRepository.createEmptyProfile();
            const user = await this.authRepository.createUser(port.user, profile);
            const cleanUser = await this.authRepository.getCleanUser(user.email);
            const token = await generateToken({
                _id: user._id,
                name: user.name,
                email: user.email
            });

            return {
                ok: true,
                data: cleanUser!,
                message: token
            }

        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: "SERVER ERROR"
            }
        }
    }
}