import {IUser} from "../../../../domain/entity/auth/IUser";
import {UserDocument} from "../../../orm/User";

export interface UpdateUserPort {
    id: UserDocument["_id"];
    user: IUser;
}