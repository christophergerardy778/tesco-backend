export interface ChangePasswordPort {
    token: string;
    password: string;
}