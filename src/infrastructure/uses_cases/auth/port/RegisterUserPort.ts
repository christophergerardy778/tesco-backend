import {UserDocument} from "../../../orm/User";

export interface RegisterUserPort {
    user: UserDocument
}