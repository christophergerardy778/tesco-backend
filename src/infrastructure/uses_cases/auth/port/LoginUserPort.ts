export interface LoginUserPort {
    email: string;
    password: string;
}