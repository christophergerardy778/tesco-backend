import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {LoginUserPort} from "./port/LoginUserPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {UserDocument} from "../../orm/User";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import {NOT_FOUND, SERVER_ERROR, UNAUTHORIZED} from "../../../shared/util/Constants";
import {stringMatch} from "../../../shared/util/HashString";
import {generateToken} from "../../../shared/util/WebToken";

export default class AdminLogin implements IBaseUseCase<LoginUserPort, HttpResponse<UserDocument>> {

    private readonly repository!: IAuthRepository;

    constructor(repository: IAuthRepository) {
        this.repository = repository;
    }

    async execute(port: LoginUserPort): Promise<HttpResponse<UserDocument>> {
        try {
            const userExistAndIsAdmin = await this.repository.getUserByEmailAndAllowRoles(port.email);

            if (userExistAndIsAdmin === null) {
                return {
                    ok: false,
                    error: NOT_FOUND.message
                }
            }

            const passwordMatch = await stringMatch(port.password, userExistAndIsAdmin.password!);

            if (!passwordMatch) {
                return {
                    ok: false,
                    error: UNAUTHORIZED.message
                }
            }

            const token = await generateToken({
                _id: userExistAndIsAdmin._id,
                name: userExistAndIsAdmin.name,
                email: userExistAndIsAdmin.email
            });

            const cleanUser = await this.repository.getCleanUser(port.email);

            return {
                ok: true,
                data: cleanUser!,
                message: token
            }

        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}