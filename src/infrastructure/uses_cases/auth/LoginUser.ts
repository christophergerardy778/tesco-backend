import {UserDocument} from "../../orm/User";
import {LoginUserPort} from "./port/LoginUserPort";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import {NOT_FOUND, SERVER_ERROR, UNAUTHORIZED} from "../../../shared/util/Constants";
import {stringMatch} from "../../../shared/util/HashString";
import {generateToken} from "../../../shared/util/WebToken";

export default class LoginUser implements IBaseUseCase<LoginUserPort, HttpResponse<UserDocument>> {

    private readonly repository!: IAuthRepository;

    constructor(repository: IAuthRepository) {
        this.repository = repository;
    }

    async execute(port: LoginUserPort): Promise<HttpResponse<UserDocument>> {
        try {
            const userExists = await this.repository.getUserByEmail(port.email);

            if (userExists === null) {
                return {
                    ok: false,
                    error: NOT_FOUND.message,
                }
            }

            const passwordIsCorrect = await stringMatch(port.password, userExists.password!);

            if (!passwordIsCorrect) {
                return {
                    ok: false,
                    error: UNAUTHORIZED.message,
                    message: "PASSWORD INCORRECT"
                }
            }

            const token = await generateToken({
                _id: userExists._id,
                name: userExists.name,
                email: userExists.email
            });

            const cleanUser = await this.repository.getCleanUser(port.email);

            return {
                ok: true,
                data: cleanUser!,
                message: token
            }
        } catch (e) {

            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }

}