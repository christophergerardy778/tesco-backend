import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {PublicationDocument} from "../../orm/Publication";
import {IPublicationRepository} from "../../../domain/repository/publication/IPublicationRepository";
import {PublicationRepository} from "../../../domain/repository/publication/PublicationRepository";
import {SERVER_ERROR} from "../../../shared/util/Constants";
import {GetPublicationsPort} from "./port/GetPublicationsPort";

export class GetPaginatedPublication
    implements IBaseUseCase<GetPublicationsPort, HttpResponse<PublicationDocument[]>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        this.repository = new PublicationRepository();
    }

    async execute(port: GetPublicationsPort): Promise<HttpResponse<PublicationDocument[]>> {
        try {
            const publications = await this.repository.getPaginatedPublication(port.page);

            return {
                ok: true,
                data: publications
            }
        } catch (e) {
            return  {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }

}