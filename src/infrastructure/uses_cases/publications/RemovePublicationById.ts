import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {RemovePublicationPort} from "./port/RemovePublicationPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IPublicationRepository} from "../../../domain/repository/publication/IPublicationRepository";
import {PublicationRepository} from "../../../domain/repository/publication/PublicationRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export class RemovePublicationById implements IBaseUseCase<RemovePublicationPort, HttpResponse<string>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        this.repository = new PublicationRepository();
    }

    async execute(port: RemovePublicationPort): Promise<HttpResponse<string>> {
        try {
            await this.repository.removePublicationById(port.publication_id);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}