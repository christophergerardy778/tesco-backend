import {ILayout} from "../../../../domain/entity/layout/ILayout";
import {UserDocument} from "../../../orm/User";
import {IPublication} from "../../../../domain/entity/publication/IPublication";

export interface CreatePublicationPort {
    user_id: UserDocument["_id"];
    publication: IPublication,
    layouts: ILayout[]
}