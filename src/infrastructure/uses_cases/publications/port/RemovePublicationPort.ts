import {PublicationDocument} from "../../../orm/Publication";

export interface RemovePublicationPort {
    publication_id: PublicationDocument["_id"];
}