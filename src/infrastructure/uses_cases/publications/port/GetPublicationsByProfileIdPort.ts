import {UserDocument} from "../../../orm/User";

export interface GetPublicationsByProfileIdPort {
    user_id: UserDocument["_id"];
}