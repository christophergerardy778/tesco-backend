import {PublicationDocument} from "../../../orm/Publication";
import {UserDocument} from "../../../orm/User";

export interface GiveOrRemoveLikePort {
    user_id: UserDocument["_id"];
    publication_id: PublicationDocument["_id"];
}