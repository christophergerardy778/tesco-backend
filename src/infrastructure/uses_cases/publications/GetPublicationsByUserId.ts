import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {GetPublicationsByProfileIdPort} from "./port/GetPublicationsByProfileIdPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {PublicationDocument} from "../../orm/Publication";
import {IPublicationRepository} from "../../../domain/repository/publication/IPublicationRepository";
import {PublicationRepository} from "../../../domain/repository/publication/PublicationRepository";
import {SERVER_ERROR} from "../../../shared/util/Constants";

export class GetPublicationsByUserId
    implements IBaseUseCase<GetPublicationsByProfileIdPort, HttpResponse<PublicationDocument[]>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        this.repository = new PublicationRepository();
    }

    async execute(port: GetPublicationsByProfileIdPort): Promise<HttpResponse<PublicationDocument[]>> {
        try {
            const result = await this.repository.getPublicationsByUserId(port.user_id);
            return {ok: true, data: result};
        } catch (e) {
            console.log(e);
            return {ok: false, error: SERVER_ERROR.message};
        }
    }
}