import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {GiveOrRemoveLikePort} from "./port/GiveOrRemoveLikePort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IPublicationRepository} from "../../../domain/repository/publication/IPublicationRepository";
import {PublicationRepository} from "../../../domain/repository/publication/PublicationRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

export class LikeManager implements IBaseUseCase<GiveOrRemoveLikePort, HttpResponse<string>>{

    private readonly repository!: IPublicationRepository;

    constructor() {
        this.repository = new PublicationRepository();
    }

    async execute(port: GiveOrRemoveLikePort): Promise<HttpResponse<string>> {
        try {
            await this.repository.updateLikeState(port.publication_id, port.user_id);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}