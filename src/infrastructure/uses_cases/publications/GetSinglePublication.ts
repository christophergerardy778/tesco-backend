import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {GetPublicationPort} from "./port/GetPublicationPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {PublicationDocument} from "../../orm/Publication";
import {IPublicationRepository} from "../../../domain/repository/publication/IPublicationRepository";
import {PublicationRepository} from "../../../domain/repository/publication/PublicationRepository";
import {NOT_FOUND, SERVER_ERROR} from "../../../shared/util/Constants";

export class GetSinglePublication implements IBaseUseCase<GetPublicationPort, HttpResponse<PublicationDocument>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        this.repository = new PublicationRepository();
    }

    async execute(port: GetPublicationPort): Promise<HttpResponse<PublicationDocument>> {
        try {

            const publicationExist = await this.repository.getSinglePublication(port.publication_id);

            if (publicationExist === null) {
                return {
                    ok: false,
                    error: NOT_FOUND.message
                }
            }

            return {
                ok: true,
                data: publicationExist
            }
        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }

}