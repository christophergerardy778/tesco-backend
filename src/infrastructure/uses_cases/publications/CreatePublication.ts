import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {CreatePublicationPort} from "./port/CreatePublicationPort";
import {IPublicationRepository} from "../../../domain/repository/publication/IPublicationRepository";
import {PublicationRepository} from "../../../domain/repository/publication/PublicationRepository";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";
import {ILayoutRepository} from "../../../domain/repository/layout/ILayoutRepository";
import LayoutRepository from "../../../domain/repository/layout/LayoutRepository";

export class CreatePublication implements IBaseUseCase<CreatePublicationPort, HttpResponse<string>> {

    private readonly layoutRepository!: ILayoutRepository;
    private readonly repository!: IPublicationRepository;

    constructor() {
        this.repository = new PublicationRepository();
        this.layoutRepository = new LayoutRepository();
    }


    async execute(port: CreatePublicationPort): Promise<HttpResponse<string>> {
        try {
            const layouts = [];
            const layoutDocuments = await this.layoutRepository.createLayouts(port.layouts);

            for (const layout of layoutDocuments) {
                layouts.push(layout._id);
            }

            await this.repository.createPublication(port.user_id, port.publication, layouts);

            return {
                ok: true,
                data: OPERATION_SUCCESS.message
            }
        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }

}