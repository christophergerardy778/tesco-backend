export interface CreateAttemptPort {
    user_id: string;
    verificationFile: string;
    graduate: boolean;
    egressDate: Date;
    enrollment: string;
}