export interface DeleteAttemptPort {
    user_id: string,
    attempt_id: string;
}