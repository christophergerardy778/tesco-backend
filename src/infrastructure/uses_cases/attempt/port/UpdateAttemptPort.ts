import {STATUS_ATTEMPT} from "../../../../domain/entity/attempt/IAttempt";

export interface DeleteAttemptPort {
    user_id: string;
    attempt_id: string;
    state: STATUS_ATTEMPT
}