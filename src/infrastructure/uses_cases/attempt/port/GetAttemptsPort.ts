export interface GetAttemptsPort {
    page: number
    status: number,
}