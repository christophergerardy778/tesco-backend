export interface GetAttemptsByUserIdPort {
    user_id: string;
}