import {AttemptDocument} from "../../orm/Attempt";
import {GetAttemptsPort} from "./port/GetAttemptsPort";
import {SERVER_ERROR} from "../../../shared/util/Constants";
import {STATUS_ATTEMPT} from "../../../domain/entity/attempt/IAttempt";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {IAttemptRepository} from "../../../domain/repository/attempt/IAttemptRepository";

export default class AdminGetAttempts implements IBaseUseCase<GetAttemptsPort, HttpResponse<AttemptDocument[]>> {

    private readonly repository: IAttemptRepository;

    constructor(repository: IAttemptRepository) {
        this.repository = repository;
    }

    async execute(port: GetAttemptsPort): Promise<HttpResponse<AttemptDocument[]>> {
        try {
            let attempts;

            switch (port.status) {
                case STATUS_ATTEMPT.ACCEPTED:
                    attempts = await this.repository.getPaginateAttempts(port.page, STATUS_ATTEMPT.ACCEPTED);
                    break;

                case STATUS_ATTEMPT.PENDING:
                    attempts = await this.repository.getPaginateAttempts(port.page, STATUS_ATTEMPT.PENDING);
                    break;

                default:
                    attempts = await this.repository.getPaginateAttempts(port.page, STATUS_ATTEMPT.REJECTED);
                    break
            }

            return {
                ok: true,
                data: attempts
            };
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}