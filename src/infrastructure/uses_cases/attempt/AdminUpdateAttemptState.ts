import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import AttemptRepository from "../../../domain/repository/attempt/AttemptRepository";
import {IAttemptRepository} from "../../../domain/repository/attempt/IAttemptRepository";
import {DeleteAttemptPort} from "./port/UpdateAttemptPort";
import {SERVER_ERROR} from "../../../shared/util/Constants";
import {STATUS_ATTEMPT} from "../../../domain/entity/attempt/IAttempt";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";


export default class AdminUpdateAttemptState implements IBaseUseCase<DeleteAttemptPort, HttpResponse<string>>{

    private readonly repository: AttemptRepository;
    private readonly authRepository!: IAuthRepository;

    constructor(repository: IAttemptRepository, authRepository: IAuthRepository) {
        this.repository = repository;
        this.authRepository = authRepository;
    }

    async execute(port: DeleteAttemptPort): Promise<HttpResponse<string>> {
        try {
            const attemptExist = await this.repository.findAttemptByIdWithUser(port.attempt_id, port.user_id);

            if (attemptExist === null) {
                return {
                    ok: false,
                    error: "ATTEMPT NO EXIST",
                }
            }

            if (port.state === STATUS_ATTEMPT.ACCEPTED) {
                await this.authRepository.updateStatus(port.user_id, true);
            }

            await this.repository.updateState(port.attempt_id, port.state);

            return {
                ok: true,
                data: "ATTEMPT_UPDATED",
                message: port.attempt_id
            };

        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }

}