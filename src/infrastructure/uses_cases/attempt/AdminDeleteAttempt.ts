import {SERVER_ERROR} from "../../../shared/util/Constants";
import {DeleteAttemptPort} from "./port/DeleteAttemptPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {IAttemptRepository} from "../../../domain/repository/attempt/IAttemptRepository";

export default class AdminDeleteAttempt implements IBaseUseCase<DeleteAttemptPort, HttpResponse<string>> {

    private readonly repository: IAttemptRepository;

    constructor(attempt: IAttemptRepository) {
        this.repository = attempt;
    }

    async execute(port: DeleteAttemptPort): Promise<HttpResponse<string>> {
        try {
            await this.repository.deleteAttempt(port.user_id, port.attempt_id);

            return {
                ok: true,
                data: "ATTEMPT_DELETED"
            };
        } catch (e) {
            return {
                ok: false,
                error: SERVER_ERROR.message,
            };
        }
    }
}