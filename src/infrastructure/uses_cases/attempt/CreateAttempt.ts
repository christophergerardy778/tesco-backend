import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {IAttemptRepository} from "../../../domain/repository/attempt/IAttemptRepository";
import {IAuthRepository} from "../../../domain/repository/auth/IAuthRepository";
import {CreateAttemptPort} from "./port/CreateAttemptPort";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {STATUS_ATTEMPT} from "../../../domain/entity/attempt/IAttempt";

export default class CreateAttempt implements IBaseUseCase<CreateAttemptPort, HttpResponse<string>> {

    private readonly repository: IAttemptRepository;
    private readonly userRepository: IAuthRepository;

    constructor(repository: IAttemptRepository, userRepository: IAuthRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    async execute(port: CreateAttemptPort): Promise<HttpResponse<string>> {
        try {
            const userNoExist = await this.userRepository.getOneUser({
                _id: port.user_id
            });

            if (userNoExist === null) {
                return {
                    ok: false,
                    error: "USER NO EXIST"
                }
            }

            const attemptCreated = await this.repository.createAttempt({
                user: port.user_id,
                verificationFile: port.verificationFile,
                status: STATUS_ATTEMPT.PENDING
            });

            await this.userRepository.updateAttemptInformation(
                port.user_id,
                port.graduate,
                port.egressDate,
                port.enrollment
            );

            return {
                ok: true,
                data: "ATTEMPT_CREATED",
                message: attemptCreated._id
            };
        } catch (e) {
            return {
                ok: false,
                error: "SERVER_ERROR"
            }
        }
    }

}