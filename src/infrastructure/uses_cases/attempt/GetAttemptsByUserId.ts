import {AttemptDocument} from "../../orm/Attempt";
import {IBaseUseCase} from "../../../domain/common/uses_cases/IBaseUseCase";
import {HttpResponse} from "../../../domain/entity/server/HttpResponse";
import {IAttemptRepository} from "../../../domain/repository/attempt/IAttemptRepository";
import {GetAttemptsByUserIdPort} from "./port/GetAttemptsByUserIdPort";
import {OPERATION_SUCCESS, SERVER_ERROR} from "../../../shared/util/Constants";

class GetAttemptsByUserId implements IBaseUseCase<GetAttemptsByUserIdPort, HttpResponse<AttemptDocument[]>> {

    private readonly repository!: IAttemptRepository;

    constructor(repository: IAttemptRepository) {
        this.repository = repository;
    }

    async execute(port: GetAttemptsByUserIdPort): Promise<HttpResponse<AttemptDocument[]>> {
        try {
            const attempts = await this.repository.getAttemptsByUserId(port.user_id);

            return {
                ok: true,
                data: attempts,
                message: OPERATION_SUCCESS.message
            }
        } catch (e) {
            console.log(e);

            return {
                ok: false,
                error: SERVER_ERROR.message
            }
        }
    }
}

export default GetAttemptsByUserId;