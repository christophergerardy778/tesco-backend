import UserRegisterSchema from "../../../../domain/entity/auth/schema/UserRegisterSchema";
import {Request, Response} from "express";

export default async function (req: Request, res: Response, next: Function) {
    try {
        await UserRegisterSchema.validateAsync(req.body);
        next();
    } catch (e) {
        return res.status(400).json({
            ok: false,
            error: "BAD_REQUEST"
        });
    }
}