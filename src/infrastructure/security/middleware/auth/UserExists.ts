import {Request, Response} from "express";
import {decodeToken} from "../../../../shared/util/WebToken";
import {UserDocument} from "../../../orm/User";
import AuthRepository from "../../../../domain/repository/auth/AuthRepository";;
import {SERVER_ERROR, UNAUTHORIZED} from "../../../../shared/util/Constants";

export default async function (req: Request, res: Response, next: Function) {
    try {
        const token = req.header("authorization");
        const data = decodeToken(token!) as UserDocument;
        const repository = new AuthRepository();

        const result = await repository.getOneUser({
            email: data.email,
            active: true
        });

        if (result !== null) return next();

        return res.status(UNAUTHORIZED.code)
            .json({
                ok: false,
                error: UNAUTHORIZED.code
            });

    } catch (e) {
        return res.status(SERVER_ERROR.code)
            .json({
                ok: false,
                error: SERVER_ERROR.message
            });
    }
}