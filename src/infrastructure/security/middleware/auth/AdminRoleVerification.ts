import {Request, Response} from "express";
import {UserDocument} from "../../../orm/User";
import AuthRepository from "../../../../domain/repository/auth/AuthRepository";
import {decodeToken} from "../../../../shared/util/WebToken";
import {SERVER_ERROR, UNAUTHORIZED} from "../../../../shared/util/Constants";

export default async function (req: Request, res: Response, next: Function) {
    try {
        const token = req.header("authorization");

        if (token === undefined) {
            return res.status(UNAUTHORIZED.code)
                .json({
                    ok: false,
                    error: UNAUTHORIZED.message
                });
        }

        const data = decodeToken(token!) as UserDocument;
        const repository = new AuthRepository();
        const result = await repository.getUserByEmailAndAllowRoles(data.email);

        if (result !== null) return next();

        return res.status(UNAUTHORIZED.code)
            .json({
                ok: false,
                error: UNAUTHORIZED.message
            });

    } catch (e) {
        return res.status(SERVER_ERROR.code)
            .json({
                ok: false,
                error: SERVER_ERROR.message
            });
    }
}