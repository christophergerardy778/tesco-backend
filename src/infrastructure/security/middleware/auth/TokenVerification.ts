import {Request, Response} from "express";
import {isValidToken} from "../../../../shared/util/WebToken";
import {BAD_REQUEST, UNAUTHORIZED} from "../../../../shared/util/Constants";

export default function (req: Request, res: Response, next: Function) {
    const token = req.header("authorization");

    if (token === undefined) {
        return res
            .status(UNAUTHORIZED.code)
            .json({
                ok: false,
                error: UNAUTHORIZED.message
            });
    }

    if (!isValidToken(token)) {
        return res
            .status(BAD_REQUEST.code)
            .json({
                ok: false,
                error: BAD_REQUEST.message
            });
    }

    return next();
}