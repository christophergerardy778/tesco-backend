import generateUUID from "../../../../shared/util/UUID";

import multer from "multer";
import path from "path";

const diskStorage = multer.diskStorage({
    destination(request: Express.Request, file: Express.Multer.File, callback: Function) {
        const dest = path.resolve(__dirname, '../../../../../public/storage/');
        callback(null, dest);
    },

    filename(req: Express.Request, file: Express.Multer.File, callback: (error: (Error | null), filename: string) => void) {
        const fileName = `${generateUUID()}${path.extname(file.originalname)}`
        callback(null, fileName);
    },
});

export default multer({
    storage: diskStorage
});