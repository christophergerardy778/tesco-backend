import {Request, Response} from "express";
import {BAD_REQUEST} from "../../../../shared/util/Constants";

export function FileExists(req: Request, res: Response, next: Function) {
    if (req.file === undefined) {
        return res
            .status(BAD_REQUEST.code)
            .json({
                ok: true,
                error: BAD_REQUEST.message
            });
    }

    return next();
}

export function FilesExists(req: Request, res: Response, next: Function) {
    if (req.files === undefined) {
        return res
            .status(BAD_REQUEST.code)
            .json({
                ok: true,
                error: BAD_REQUEST.message
            });
    }

    return next();
}