import {Document, model, Schema} from "mongoose";
import {IPublication} from "../../domain/entity/publication/IPublication";

export interface PublicationDocument extends Document, IPublication {}

const schema = new Schema({
    title: {
        type: String,
        required: [true, "title required"]
    },

    description: {
        type: String,
        required: [true, 'Description required']
    },

    cover: {
        type: String
    },

    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User required"]
    },

    layouts: [{
        type: Schema.Types.ObjectId,
        ref: "Layout",
        default: []
    }],
    likes: [{
        type: Schema.Types.ObjectId,
        ref: "User",
        default: []
    }]
}, {
    timestamps: true
});

export default model<PublicationDocument>("Publication", schema);