import { IFile } from "../../domain/entity/files/IFile";

import {Document, model, Schema} from "mongoose";

export interface FileDocument extends Document, IFile {}

const schema = new Schema({
    name: {
        type: String,
        required: [true, 'file name required'],
    },

    type: {
        type: Number,
        required: [true, 'File type required']
    }
});

export default model<FileDocument>('File', schema);