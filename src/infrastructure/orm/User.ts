import {Document, Schema, model} from "mongoose";
import {IUser, Role} from "../../domain/entity/auth/IUser";
import {generateHash} from "../../shared/util/HashString";

export interface UserDocument extends Document, IUser {}

const userSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name required']
    },

    lastname: {
        type: String,
        required: [true, 'Lastname required']
    },

    email: {
        type: String,
        required: [true, 'Email required']
    },

    password: {
        type: String,
        required: [true, 'Password required']
    },

    active: {
        type: Boolean,
        default: false
    },

    enrollment: {
        type: String,
        default: null
    },

    roll: {
        type: Number,
        default: Role.STUDENT
    },

    graduate: {
        type: Boolean,
        default: false
    },

    dateOfAdmission: {
        type: Date,
        default: null
    },

    egressDate: {
        type: Date,
        default: null
    },

    profile: {
        type: Schema.Types.ObjectId,
        ref: 'Profile'
    }
}, {
    timestamps: true
});

userSchema.pre<UserDocument>("save", async function (next) {
    if (!this.isModified("password")) return next();
    this.password = await generateHash(this.password!);
    return next();
});

export default model<UserDocument>("User", userSchema);
