import mongoose from "mongoose";

export default async function createConnection() {
    const mongooseResult = await mongoose.connect(process.env.MONGO_URL!, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    return mongooseResult.connection;
};

