import {IAttempt, STATUS_ATTEMPT} from "../../domain/entity/attempt/IAttempt";

import {Document, model, Schema} from "mongoose";

export interface AttemptDocument extends Document, IAttempt {}

const schema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User id required"]
    },

    verificationFile: {
        type: String,
        required: [true, "Verification file required"]
    },

    status: {
        type: Number,
        default: STATUS_ATTEMPT.PENDING
    }
}, {
    timestamps: true
});

export default model<AttemptDocument>("Attempt", schema);