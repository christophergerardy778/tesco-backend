import {IExperience} from "../../domain/entity/experience/IExperience";
import {Document, Schema, model} from "mongoose";

export interface ExperienceDocument extends IExperience, Document {}

const schema = new Schema({
    company: {
        type: String,
        required: true
    },
    job: {
        type: String,
        required: true
    },
    start_at: {
        type: Date,
        required: true
    },
    finish_at: {
        type: Date,
        required: false
    },
    description: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

export default model<ExperienceDocument>('Experience', schema);