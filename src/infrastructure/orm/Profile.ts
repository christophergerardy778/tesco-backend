import {IProfile} from "../../domain/entity/profile/IProfile";
import {Document, model, Schema} from "mongoose";

export interface ProfileDocument extends IProfile, Document {}

const schema = new Schema({
    web_url: {
        type: String,
        required: false,
    },
    facebook: {
        type: String,
        required: false
    },
    linkedin: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    has_job: {
        type: Boolean,
        default: false
    },
    job: {
        type: String,
        required: false,
    },
    company: {
        type: String,
        required: false
    },
    skills: [{
        type: Schema.Types.ObjectId,
        ref: "Skills",
        required: false
    }],
    experience: [{
        type: Schema.Types.ObjectId,
        ref: "Experience",
        required: false
    }]
}, {
    timestamps: true
});

export default model<ProfileDocument>('Profile', schema);