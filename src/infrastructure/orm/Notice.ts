import {INotice} from "../../domain/entity/notice/INotice";

import {Document, model, Schema} from "mongoose";

export interface NoticeDocument extends Document, INotice {}

const schema = new Schema({
    title: {
        type: String,
        required: [true, "title required"]
    },

    description: {
        type: String,
        required: [true, 'Description required']
    },

    cover: {
        type: String
    },

    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User required"]
    },

    layouts: [{
        type: Schema.Types.ObjectId,
        ref: "Layout",
        required: false,
        default: []
    }]
}, {
    timestamps: true
});

export default model<NoticeDocument>("Notice", schema);