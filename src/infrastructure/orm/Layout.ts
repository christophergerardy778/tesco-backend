import { ILayout } from "../../domain/entity/layout/ILayout";

import {Document, model, Schema} from "mongoose";

export interface LayoutDocument extends Document, ILayout {}

const schema = new Schema({
    type: {
        type: Number,
        required: [true, "Layout type required"]
    },

    priority: {
        type: Number,
        required: [true, "Layout priority required"]
    },

    title: {
        type: String,
        default: ""
    },

    description: {
        type: String,
        default: ""
    },

    source: {
        type: Schema.Types.ObjectId,
        ref: "File",
        required: false
    },

    sources: [{
        type: Schema.Types.ObjectId,
        ref: "File",
        required: false
    }],

    links: [{
        type: String,
        required: false
    }]
});

export default model<LayoutDocument>("Layout", schema);