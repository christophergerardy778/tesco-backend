import {ISkill} from "../../domain/entity/skills/ISkill";
import {Document, model, Schema} from "mongoose";

export interface SkillDocument extends ISkill, Document {}

const schema = new Schema({
    name: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

export default model<SkillDocument>('Skills', schema);