import { compare, hash, genSalt } from "bcryptjs";

export async function generateHash(text: string) {
    const salt = await genSalt(10);
    return hash(text, salt);
}

export async function stringMatch(text: string, hash: string) {
    return compare(text, hash);
}