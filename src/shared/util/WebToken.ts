import { sign, verify } from "jsonwebtoken";

export function generateToken(payload: any) {
    return sign(payload, process.env.TOKEN_SIGN!, {
        expiresIn: "180d"
    });
}

export function generateTokenWithLimit(payload: any, expiration: string) {
    return sign(payload, process.env.TOKEN_SIGN!, {
        expiresIn: expiration
    });
}

export function isValidToken(token: string) : boolean{
    try {
        verify(token, process.env.TOKEN_SIGN!);
        return true;
    } catch (e) {
        return false;
    }
}

export function decodeToken(token: string) {
    return verify(token, process.env.TOKEN_SIGN!);
}