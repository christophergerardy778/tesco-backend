export const OPERATION_SUCCESS = {
    code: 200,
    message: "OPERATION SUCCESS"
};

export const BAD_REQUEST = {
    code: 400,
    message: "BAD REQUEST"
};

export const NOT_FOUND = {
    code: 404,
    message: "NOT FOUND"
};

export const UNAUTHORIZED = {
    code: 401,
    message: "UNAUTHORIZED"
};

export const SERVER_ERROR = {
    code: 500,
    message: "SERVER ERROR"
};

export const IMAGE_TYPES = [
    '.jpg',
    '.jpeg',
    '.png'
];

export const VIDEO_TYPES = [
    '.mp4',
    '.mvk',
    '.flv',
    '.3gp'
];

export const FILE_PDF = '.pdf';
export const WORD_DOC = '.docx';
export const WORD_CLASSIC_DOC = '.doc';
