import {v4 as uuid} from "uuid";

export default function generateUUID() {
    return uuid();
}