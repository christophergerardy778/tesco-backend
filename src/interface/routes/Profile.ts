import {Router} from "express";
import TokenVerification from "../../infrastructure/security/middleware/auth/TokenVerification";
import ProfileController from "../controller/ProfileController";
import FileVerification from "../../infrastructure/security/middleware/file/FileVerification";

const router = Router();

router.get("/:id", [TokenVerification], ProfileController.getProfileById);
router.put("/update", [TokenVerification], ProfileController.updateProfile);
router.put("/skills", [TokenVerification], ProfileController.updateSkills);

router.post("/experience", [TokenVerification], ProfileController.addExperience);
router.put("/experience", [TokenVerification], ProfileController.updateExperience);
router.delete("/experience",  [TokenVerification], ProfileController.deleteExperience);

router.put("/photo", [
    TokenVerification,
    FileVerification.single("photo")
], ProfileController.updateProfilePhoto);

export default router;