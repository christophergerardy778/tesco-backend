import AdminRoleVerification from "../../infrastructure/security/middleware/auth/AdminRoleVerification";
import TokenVerification from "../../infrastructure/security/middleware/auth/TokenVerification";
import FileVerification from "../../infrastructure/security/middleware/file/FileVerification";
import {FileExists} from "../../infrastructure/security/middleware/file/FileExists";
import AttemptController from "../controller/AttemptController";

import {Router} from "express";

const router = Router();

router.post("/", [
    TokenVerification,
    FileVerification.single("file"),
    FileExists
], AttemptController.createAttempt);

router.get("/", [
    TokenVerification,
    AdminRoleVerification
], AttemptController.getAttempts);

router.get("/:id", [
    TokenVerification
], AttemptController.getAttemptsByUserId);

router.put("/", [
    TokenVerification,
    AdminRoleVerification
], AttemptController.updateAttempt);

router.delete("/", [
    TokenVerification,
    AdminRoleVerification
], AttemptController.deleteAttempt);

export default router;