import {Router} from "express";
import AdminRoleVerification from "../../infrastructure/security/middleware/auth/AdminRoleVerification";
import UserController from "../controller/UserController";

const router = Router();

router.get("/find", [AdminRoleVerification], UserController.findUserByEmail);
router.put("/roll", [AdminRoleVerification], UserController.changeUserRoll);
router.get("/", [AdminRoleVerification], UserController.getUsersByRoll);
router.put("/password", [AdminRoleVerification], UserController.updatePassword);

export default router;