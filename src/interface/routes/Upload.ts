import TokenVerification from "../../infrastructure/security/middleware/auth/TokenVerification";
import FileVerification from "../../infrastructure/security/middleware/file/FileVerification";
import UploadController from "../controller/UploadController";
import {Router} from "express";
import UserExists from "../../infrastructure/security/middleware/auth/UserExists";

const router = Router();

router.post("/", [
    TokenVerification,
    UserExists,
    FileVerification.array("files")
], UploadController.uploadFile);

router.delete("/", [
    TokenVerification,
    UserExists
], UploadController.deleteFile);

export default router;