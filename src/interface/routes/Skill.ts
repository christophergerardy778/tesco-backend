import {Router} from "express";
import SkillController from "../controller/SkillController";
import TokenVerification from "../../infrastructure/security/middleware/auth/TokenVerification";

const router = Router();

router.get("/", [TokenVerification], SkillController.getAllSkills);
router.post("/", [TokenVerification], SkillController.createSkill);
router.put("/:id", [TokenVerification], SkillController.updateSkill);
router.delete("/:id", [TokenVerification], SkillController.deleteSkill);

export default router;