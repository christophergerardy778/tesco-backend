import {Router} from "express";
import ChartsController from "../controller/ChartsController";

const router = Router();

router.get("/", ChartsController.getCharts);

export default router;