import {Router} from "express";
import PublicationController from "../controller/PublicationController";
import TokenVerification from "../../infrastructure/security/middleware/auth/TokenVerification";

const router = Router();

router.get("/", [TokenVerification], PublicationController.getPublicationsPaginated);
router.get("/post/:id", [TokenVerification], PublicationController.getSinglePublication);
router.get("/user/:id", [TokenVerification], PublicationController.getPublicationsByUserId);

router.put("/like", [TokenVerification], PublicationController.changeLike);

router.post("/", [TokenVerification], PublicationController.createPublication);
router.post("/upload", [TokenVerification], PublicationController.uploadPhotos);

router.delete("/", [TokenVerification], PublicationController.deletePublication);

export default router;