import {Router} from "express";
import TokenVerification from "../../infrastructure/security/middleware/auth/TokenVerification";
import NoticeController from "../controller/NoticeController";
import AdminRoleVerification from "../../infrastructure/security/middleware/auth/AdminRoleVerification";
import UserExists from "../../infrastructure/security/middleware/auth/UserExists";

const router = Router();

router.post("/", [
    TokenVerification,
    UserExists,
    AdminRoleVerification
], NoticeController.createNotice);

router.get("/", [], NoticeController.getNotices);

router.get("/:id", [], NoticeController.getNoticeById);

router.delete("/:id", [
    TokenVerification,
    UserExists,
    AdminRoleVerification
], NoticeController.deleteNotice);

export default router;