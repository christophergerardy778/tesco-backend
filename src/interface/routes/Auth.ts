import UserSchemaVerification from "../../infrastructure/security/middleware/auth/UserSchemaVerification";
import AuthController from "../controller/AuthController";

import {Router} from "express";
import TokenVerification from "../../infrastructure/security/middleware/auth/TokenVerification";

const router = Router();

router.post("/login", AuthController.loginUser);
router.post("/register", [UserSchemaVerification], AuthController.registerUser);
router.post("/reset-password", AuthController.resetPassword);
router.post("/change-password", AuthController.changePassword);
router.get("/:token", [TokenVerification], AuthController.getUserByToken);
router.put("/user", [TokenVerification], AuthController.updatePersonalData);

router.post("/admin", AuthController.adminLogin);

export default router;