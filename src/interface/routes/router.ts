import express from "express";

import Attempt from "./Attempt";
import Upload from "./Upload";
import Notice from "./Notice";
import Auth from "./Auth";
import Skill from "./Skill";
import Profile from "./Profile";
import Publication from "./Publication";
import Charts from "./Charts";
import User from "./User";

const app = express();

app.use("/auth", Auth);
app.use("/upload", Upload);
app.use("/notice", Notice);
app.use("/attempt", Attempt);
app.use("/skill", Skill);
app.use("/profile", Profile);
app.use("/publication", Publication);
app.use("/charts", Charts);
app.use("/user", User);

export default app;