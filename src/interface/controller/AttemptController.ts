import AuthRepository from "../../domain/repository/auth/AuthRepository";
import AttemptRepository from "../../domain/repository/attempt/AttemptRepository";
import AdminGetAttempts from "../../infrastructure/uses_cases/attempt/AdminGetAttempts";
import CreateAttempt from "../../infrastructure/uses_cases/attempt/CreateAttempt";
import AdminDeleteAttempt from "../../infrastructure/uses_cases/attempt/AdminDeleteAttempt";
import AdminUpdateAttemptState from "../../infrastructure/uses_cases/attempt/AdminUpdateAttemptState";

import {Request, Response} from "express";
import GetAttemptsByUserId from "../../infrastructure/uses_cases/attempt/GetAttemptsByUserId";

export default {
    async createAttempt(req: Request, res: Response) {
        const repository = new AttemptRepository();
        const userRepository = new AuthRepository();
        const useCase = new CreateAttempt(repository, userRepository);

        const result = await useCase.execute({
            user_id: req.body.user_id,
            enrollment: req.body.enrollment,
            verificationFile: req.file.filename,
            graduate: req.body.graduate === 'true',
            egressDate: new Date(req.body.egressDate)
        });

        return res.json(result);
    },

    async getAttempts(req: Request, res: Response) {
        const useCase = new AdminGetAttempts(new AttemptRepository());

        const result = await useCase.execute({
            page: Number.parseInt(req.query.page as string),
            status: Number.parseInt(req.query.status! as string)
        });

        return res.json(result);
    },

    async getAttemptsByUserId(req: Request, res: Response) {
        const useCase = new GetAttemptsByUserId(new AttemptRepository());

        const result = await useCase.execute({
            user_id: req.params.id
        });

        return res.json(result);
    },

    async deleteAttempt(req: Request, res: Response) {
        const useCase = new AdminDeleteAttempt(new AttemptRepository());

        const result = await useCase.execute({
            user_id: req.body.user_id,
            attempt_id: req.body.attempt_id
        });

        return res.json(result);
    },

    async updateAttempt(req: Request, res: Response) {
        const useCase = new AdminUpdateAttemptState(new AttemptRepository(), new AuthRepository());

        const result = await useCase.execute({
            user_id: req.body.user_id,
            attempt_id: req.body.attempt_id,
            state: req.body.state
        })

        return res.json(result);
    }
}