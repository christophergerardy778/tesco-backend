import RegisterUser from "../../infrastructure/uses_cases/auth/RegisterUser";
import AuthRepository from "../../domain/repository/auth/AuthRepository";
import {Request, Response} from "express";
import LoginUser from "../../infrastructure/uses_cases/auth/LoginUser";
import ResetPassword from "../../infrastructure/uses_cases/auth/ResetPassword";
import ChangePassword from "../../infrastructure/uses_cases/auth/ChangePassword";
import AdminLogin from "../../infrastructure/uses_cases/auth/AdminLogin";
import GetUserByToken from "../../infrastructure/uses_cases/auth/GetUserByToken";
import ProfileRepository from "../../domain/repository/profile/ProfileRepository";
import {UpdateUser} from "../../infrastructure/uses_cases/auth/UpdateUser";

export default {
    async registerUser(req: Request, res: Response) {

        const authInteraction = new AuthRepository();
        const profileRepository = new ProfileRepository();
        const userUseCase = new RegisterUser(authInteraction, profileRepository);

        const result = await userUseCase.execute({
            user: req.body
        });

        return res.json(result);
    },

    async loginUser(req: Request, res: Response) {
        const useCase = new LoginUser(new AuthRepository());

        const result = await useCase.execute({
            email: req.body.email,
            password: req.body.password
        });

        return res.json(result);
    },

    async resetPassword(req: Request, res: Response) {
        const useCase = new ResetPassword(new AuthRepository());

        const result = await useCase.execute({
            email: req.body.email
        });

        return res.json(result);
    },

    async changePassword(req: Request, res: Response) {
        const useCase = new ChangePassword(new AuthRepository());

        const result = await useCase.execute({
            token: req.body.token,
            password: req.body.password
        });

        return res.json(result);
    },

    async adminLogin(req: Request, res: Response) {
        const useCase = new AdminLogin(new AuthRepository());

        const result = await useCase.execute({
            email: req.body.email,
            password: req.body.password
        });

        return res.json(result);
    },

    async getUserByToken(req: Request, res: Response) {
        const useCase = new GetUserByToken(new AuthRepository());

        const result = await useCase.execute({
            token: req.params.token
        });

        return res.json(result);
    },

    async updatePersonalData(req: Request, res: Response) {
        const useCase = new UpdateUser();

        const result = await useCase.execute({
            id: req.body.id,
            user: req.body.user
        });

        return res.json(result);
    }
};