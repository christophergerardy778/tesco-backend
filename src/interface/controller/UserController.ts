import {Request, Response} from "express";
import {HttpResponse} from "../../domain/entity/server/HttpResponse";
import {UserDocument} from "../../infrastructure/orm/User";
import AuthRepository from "../../domain/repository/auth/AuthRepository";
import {NOT_FOUND, OPERATION_SUCCESS, SERVER_ERROR} from "../../shared/util/Constants";
import {generateHash} from "../../shared/util/HashString";

export default {
    async findUserByEmail(req: Request, res: Response): Promise<Response<HttpResponse<UserDocument>>> {
        const repository = new AuthRepository();
        const user = await repository.getCleanUser(req.query.email as string);

        if (user === null) {
            return res.status(NOT_FOUND.code)
                .json({
                    ok: false,
                    error: NOT_FOUND.message
                });
        }

        return res.status(OPERATION_SUCCESS.code)
            .json({
                ok: true,
                data: user
            });
    },

    async changeUserRoll(req: Request, res: Response): Promise<Response<HttpResponse<string>>> {
        try {
            const repository = new AuthRepository();
            const user = await repository.getUserByEmail(req.body.email);

            if (user === null) {
                return res.status(NOT_FOUND.code)
                    .json({
                        ok: false,
                        error: NOT_FOUND.message
                    });
            }

            user.roll = req.body.roll;
            await user.save();

            return res.status(OPERATION_SUCCESS.code)
                .json({
                    ok: true,
                    message: OPERATION_SUCCESS.message
                });
        } catch (e) {
            return res.status(SERVER_ERROR.code)
                .json({
                    ok: false,
                    error: SERVER_ERROR.message
                });
        }
    },

    async getUsersByRoll(req: Request, res: Response): Promise<Response<HttpResponse<UserDocument[]>>> {
        try {
            const repository = new AuthRepository();
            const users = await repository.getUsersByRoleModerator();

            return res.status(OPERATION_SUCCESS.code)
                .json({
                    ok: true,
                    data: users
                });
        } catch (e) {
            return res.status(SERVER_ERROR.code)
                .json({
                    ok: false,
                    error: SERVER_ERROR.message
                });
        }
    },

    async updatePassword(req: Request, res: Response): Promise<Response<HttpResponse<string>>> {
        try {
            const repository = new AuthRepository();
            const hash = await generateHash(req.body.password);
            await repository.updatePassword(req.body.password, hash);

            return res.status(OPERATION_SUCCESS.code)
                .json({
                    ok: true,
                    message: OPERATION_SUCCESS.message
                });
        } catch (e) {
            return res.status(SERVER_ERROR.code)
                .json({
                    ok: false,
                    error: SERVER_ERROR.message
                });
        }
    }
}