import {Request, Response} from "express";
import GetProfileById from "../../infrastructure/uses_cases/profile/GetProfileById";
import {UpdateProfile} from "../../infrastructure/uses_cases/profile/UpdateProfile";
import {UpdateProfilePhoto} from "../../infrastructure/uses_cases/profile/UpdateProfilePhoto";
import {UpdateSkills} from "../../infrastructure/uses_cases/profile/UpdateSkills";
import {AddExperience} from "../../infrastructure/uses_cases/profile/AddExperience";
import {DeleteExperience} from "../../infrastructure/uses_cases/profile/DeleteExperience";
import {UpdateExperience} from "../../infrastructure/uses_cases/profile/UpdateExperience";

export default {
    async getProfileById(req: Request, res: Response) {
        const useCase = new GetProfileById();

        const result = await useCase.execute({
            user_id: req.params.id
        });

        return res.json(result);
    },

    async updateProfile(req: Request, res: Response) {
        const useCase = new UpdateProfile();

        const result = await useCase.execute({
            id: req.body.id,
            profile: req.body.profile
        });

        return res.json(result);
    },

    async updateProfilePhoto(req: Request, res: Response) {
        const useCase = new UpdateProfilePhoto();

        const result = await useCase.execute({
            id: req.body.id,
            photo: req.file
        });

        return res.json(result);
    },

    async updateSkills(req: Request, res: Response) {
        const useCase = new UpdateSkills();

        const result = await useCase.execute({
            id: req.body.id,
            skills: req.body.skills
        })

        return res.json(result);
    },

    async addExperience(req: Request, res: Response) {
        const useCase = new AddExperience();

        const result = await useCase.execute({
            id: req.body.id,
            experience: req.body.experience
        });

        return res.json(result);
    },

    async deleteExperience(req: Request, res: Response) {
        const useCase = new DeleteExperience();

        const result = await useCase.execute({
            id: req.body.id,
            profile_id: req.body.profile_id
        });

        return res.json(result);
    },

    async updateExperience(req: Request, res: Response) {
        const useCase = new UpdateExperience();

        const result = await useCase.execute({
            id: req.body.id,
            experience: req.body.experience
        });

        return res.json(result);
    }
}