import {Request, Response} from "express";
import CreateNotice from "../../infrastructure/uses_cases/notice/CreateNotice";
import NoticeRepository from "../../domain/repository/notice/NoticeRepository";
import LayoutRepository from "../../domain/repository/layout/LayoutRepository";
import GetNotices from "../../infrastructure/uses_cases/notice/GetNotices";
import GetNoticeById from "../../infrastructure/uses_cases/notice/GetNoticeById";
import DeleteNotice from "../../infrastructure/uses_cases/notice/DeleteNotice";

export default {
    async createNotice(req: Request, res: Response) {
        const useCase = new CreateNotice(new NoticeRepository(), new LayoutRepository());

        const result = await useCase.execute({
            user_id: req.body.user_id,
            notice: req.body.notice,
            layouts: req.body.layouts
        });

        return res.json(result);
    },

    async getNotices(req: Request, res: Response) {
        const useCase = new GetNotices(new NoticeRepository());

        const result = await useCase.execute({
            page: Number.parseInt(req.query.page as string)
        });

        return res.json(result);
    },

    async getNoticeById(req: Request, res: Response) {
        const useCase = new GetNoticeById(new NoticeRepository());

        const result = await useCase.execute({
            notice_id: req.params.id
        });

        return res.json(result);
    },

    async updateNotice(req: Request, res: Response) {

    },

    async deleteNotice(req: Request, res: Response) {
        const useCase = new DeleteNotice(new NoticeRepository());

        const result = await useCase.execute({
            notice_id: req.params.id
        });

        return res.json(result);
    }
}