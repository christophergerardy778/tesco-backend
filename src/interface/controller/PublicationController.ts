import {Request, Response} from "express";
import {CreatePublication} from "../../infrastructure/uses_cases/publications/CreatePublication";
import {GetPaginatedPublication} from "../../infrastructure/uses_cases/publications/GetPaginatedPublications";
import {RemovePublicationById} from "../../infrastructure/uses_cases/publications/RemovePublicationById";
import {GetSinglePublication} from "../../infrastructure/uses_cases/publications/GetSinglePublication";
import {LikeManager} from "../../infrastructure/uses_cases/publications/LikeManager";
import {GetPublicationsByUserId} from "../../infrastructure/uses_cases/publications/GetPublicationsByUserId";

export default {

    async getPublicationsPaginated(req: Request, res: Response) {
        const useCase = new GetPaginatedPublication();

        const result = await useCase.execute({
            page: Number.parseInt(req.query.page as string)
        });

        return res.json(result);
    },

    async getSinglePublication(req: Request, res: Response) {
        const useCase = new GetSinglePublication();

        const result = await useCase.execute({
            publication_id: req.params.id
        });

        return res.json(result);
    },

    async createPublication(req: Request, res: Response) {
        const useCase = new CreatePublication();

        const result = await useCase.execute({
            publication: req.body.publication,
            user_id: req.body.user_id,
            layouts: req.body.layouts
        });

        return res.json(result);
    },

    async uploadPhotos(req: Request, res: Response) {

    },

    async deletePublication(req: Request, res: Response) {
        const useCase = new RemovePublicationById();

        const result = await useCase.execute({
            publication_id: req.body.publication_id
        });

        return res.json(result);
    },

    async changeLike(req: Request, res: Response) {
        const useCase = new LikeManager();

        const result = await useCase.execute({
            publication_id: req.body.publication_id,
            user_id: req.body.user_id
        });

        return res.json(result);
    },

    async getPublicationsByUserId(req: Request, res: Response) {
        const useCase = new GetPublicationsByUserId();
        const result = await useCase.execute({user_id: req.params.id});
        return res.json(result);
    },

    async updatePublication(req: Request, res: Response) {

    }
}