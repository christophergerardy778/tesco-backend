import {Request, Response} from "express";
import UploadFile from "../../infrastructure/uses_cases/upload/UploadFile";
import UploadRepository from "../../domain/repository/upload/UploadRepository";
import DeleteFile from "../../infrastructure/uses_cases/upload/DeleteFile";

export default {
    async uploadFile(req: Request, res: Response) {
        const useCase = new UploadFile(new UploadRepository());

        const result = await useCase.execute({
            files: req.files as Express.Multer.File[]
        });

        return res.json(result);
    },

    async deleteFile(req: Request, res: Response) {
        const useCase = new DeleteFile(new UploadRepository());

        const result = await useCase.execute({
            fileIdArray: req.body.files as string[]
        });

        return res.json(result);
    }
}