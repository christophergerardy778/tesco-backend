import {Request, Response} from "express";
import CreateSkill from "../../infrastructure/uses_cases/skill/CreateSkill";
import GetAllSkills from "../../infrastructure/uses_cases/skill/GetAllSkills";
import UpdateSkill from "../../infrastructure/uses_cases/skill/UpdateSkill";
import DeleteSkill from "../../infrastructure/uses_cases/skill/DeleteSkill";

export default {
    async getAllSkills(req: Request, res: Response) {
        const useCase = new GetAllSkills();
        const result = await useCase.execute({});
        return res.json(result);
    },

    async createSkill(req: Request, res: Response) {
        const useCase = new CreateSkill();

        const result = await useCase.execute({
            name: req.body.name,
            color: req.body.color
        });

        return res.json(result);
    },

    async updateSkill(req: Request, res: Response) {
        const useCase = new UpdateSkill();

        const result = await useCase.execute({
            skill_id: req.params.id,
            color: req.body.color,
            name: req.body.name
        });

        return res.json(result);
    },

    async deleteSkill(req: Request, res: Response) {
        const useCase = new DeleteSkill();
        const result = await useCase.execute({ skill_id: req.params.id });
        return res.json(result);
    }
}