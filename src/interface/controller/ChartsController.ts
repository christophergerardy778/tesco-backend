import {Request, Response} from "express";
import {GetCharts} from "../../infrastructure/uses_cases/charts/GetCharts";

export default {
    async getCharts(req: Request, res: Response) {
        const useCase = new GetCharts();
        const result = await useCase.execute({});
        return res.json(result);
    }
}