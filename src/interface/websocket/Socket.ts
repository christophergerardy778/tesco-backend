import {Server, Socket} from "socket.io";
import * as http from "http";

export default function WebSocket(httpServer: http.Server) {
    const io = new Server(httpServer);

    io.on("connection", (socket: Socket) => {
        socket.on("user-connected-to-call", (userId, socketId) => {
            socket.join("principal-room");
            socket.broadcast.to("principal-room").emit("newUser", userId, socketId)
        });

        socket.on("message-text", (text) => {
            socket.broadcast.to("principal-room").emit("newMessage", text)
        });

        socket.on("disconnect", () => {
            socket.broadcast.to("principal-room").emit("userLeave", socket.id);
        });
    })
}