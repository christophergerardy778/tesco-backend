import { ExpressPeerServer } from "peer";
import { Server } from "http";

export default function createPeerServer(app: Server) {
    return ExpressPeerServer(app);
}