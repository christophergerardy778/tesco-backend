export interface IBaseUseCase <IPort, IResult> {
    execute(port: IPort) : Promise<IResult>;
}