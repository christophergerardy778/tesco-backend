export enum IFileTypes {
    IMAGE,
    VIDEO,
    PDF,
    DOC
}

export interface IFile {
    file_id: string;
    name: string;
    type: IFileTypes;
}