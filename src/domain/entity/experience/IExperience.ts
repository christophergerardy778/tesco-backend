export interface IExperience {
    company: string;
    job: string;
    start_at: Date;
    finish_at: Date;
    description: string;
}