export enum NotificationType {
    NoticeNotification,
    PublicationNotification
}

export interface NotificationBuild {
    type: NotificationType,
    source_id: string;
    tokens: string[],
    title: string,
    body: string,
    imageUrl?: string,
}