export interface HttpResponse<T> {
    ok: boolean,
    data?: T,
    message?: string;
    error?: string;
}