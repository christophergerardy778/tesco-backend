export interface ISkillResult {
    technology: string,
    usages: number
}