import {ISkill} from "../skills/ISkill";
import {IExperience} from "../experience/IExperience";

export interface IProfile {
    web_url: string;
    facebook: string;
    linkedin: string;
    photo: string;
    description: string;

    has_job: boolean;
    job: string;
    company: string;

    skills: ISkill[];
    experience: IExperience[];
}