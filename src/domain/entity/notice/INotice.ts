import {ILayout} from "../layout/ILayout";
import {UserDocument} from "../../../infrastructure/orm/User";

export interface INotice {
    notice_id: string;
    title: string;
    description: string;
    cover?: string;
    user: UserDocument["_id"];
    layouts: ILayout[];
}