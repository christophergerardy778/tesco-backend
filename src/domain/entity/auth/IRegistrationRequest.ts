import {UserDocument} from "../../../infrastructure/orm/User";

export enum Status {
    PENDING,
    ACCEPTED,
    REJECTED
}

export interface IRegistrationRequest {
    user_id: UserDocument["_id"];
    verificationFile: String;
    status: Status;
    created: Date;
}
