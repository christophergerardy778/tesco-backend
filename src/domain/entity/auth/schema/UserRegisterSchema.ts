import joi from "joi";

export default joi.object({
    name: joi.string()
        .required(),

    lastname: joi.string()
        .required(),

    email: joi.string()
        .email()
        .required(),

    password: joi.string()
        .min(6)
        .required(),

    graduate: joi.boolean()
        .required(),

    dateOfAdmission: joi.date()
        .required(),

    enrollment: joi.string()
});