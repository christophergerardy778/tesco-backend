import {IProfile} from "../profile/IProfile";

export enum Role {
    ADMIN,
    MODERATOR,
    TEACHER,
    STUDENT,
}

export interface IUser {
    user_id?: string;
    name: string;
    lastname: string;
    email: string;
    password?: string;
    active: boolean;
    enrollment?: string;
    roll: Role;
    graduate: boolean;
    dateOfAdmission?: Date;
    egressDate?: Date;
    profile: IProfile;
}

