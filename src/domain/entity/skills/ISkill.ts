export interface ISkill {
    name: string;
    color: string;
}