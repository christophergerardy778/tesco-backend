import {IFile} from "../files/IFile";

export interface ILayout {
    type: number;
    priority: number;

    title: string;
    description: string;
    source: IFile;

    links: string[];
    sources: IFile[];
}