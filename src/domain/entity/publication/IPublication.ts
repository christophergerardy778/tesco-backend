import {UserDocument} from "../../../infrastructure/orm/User";
import {LayoutDocument} from "../../../infrastructure/orm/Layout";

export interface IPublication {
    title: string;
    description: string;
    cover: string;
    user: UserDocument["_id"];
    layouts: LayoutDocument["_id"][];
    likes: UserDocument["_id"][];
}