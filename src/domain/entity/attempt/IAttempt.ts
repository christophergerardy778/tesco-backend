import {UserDocument} from "../../../infrastructure/orm/User";

export enum STATUS_ATTEMPT {
    PENDING,
    REJECTED,
    ACCEPTED
}

export interface IAttempt {
    user: UserDocument["_id"],
    verificationFile: string,
    status: STATUS_ATTEMPT;
}