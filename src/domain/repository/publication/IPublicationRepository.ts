import {PublicationDocument} from "../../../infrastructure/orm/Publication";
import {UserDocument} from "../../../infrastructure/orm/User";
import {IPublication} from "../../entity/publication/IPublication";
import {LayoutDocument} from "../../../infrastructure/orm/Layout";

export interface IPublicationRepository {
    createPublication(
        user: UserDocument["_id"],
        publication: IPublication,
        layouts: LayoutDocument["_id"][]
    ): Promise<PublicationDocument>;

    getPaginatedPublication(page: number): Promise<PublicationDocument[]>;
    getPublicationsByUserId(user_id: UserDocument["_id"]) : Promise<PublicationDocument[]>
    getSinglePublication(publication_id: PublicationDocument["_id"]): Promise<PublicationDocument | null>;

    removePublicationById(id: PublicationDocument["_id"]) : Promise<void>;

    updateLikeState(publication_id: PublicationDocument["id"], user_id: UserDocument["_id"]): Promise<void>;
}