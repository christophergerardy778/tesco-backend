import {IPublicationRepository} from "./IPublicationRepository";
import Publication, {PublicationDocument} from "../../../infrastructure/orm/Publication";
import {IPublication} from "../../entity/publication/IPublication";
import {LayoutDocument} from "../../../infrastructure/orm/Layout";
import {UserDocument} from "../../../infrastructure/orm/User";

export class PublicationRepository implements IPublicationRepository {
    createPublication(
        user: UserDocument["_id"],
        publication: IPublication,
        layouts: LayoutDocument["_id"][]
    ): Promise<PublicationDocument> {
        return Publication.create({
            title: publication.title,
            cover: publication.cover,
            user: user,
            description: publication.description,
            layouts: layouts,
        });
    }

    getPaginatedPublication(page: number): Promise<PublicationDocument[]> {
        return Publication.find()
            .populate({
                path: 'user',
                populate: [
                    {path: 'profile'}
                ]
            })
            .populate({
                path: 'layouts',

                populate: [
                    {path: "source"},
                    {path: "sources"}
                ]
            })
            .skip(10 * page)
            .limit(10)
            .sort({createdAt: "desc"})
            .exec();
    }

    async removePublicationById(id: PublicationDocument["_id"]): Promise<void> {
        await Publication.deleteOne({
            _id: id
        });
    }

    getSinglePublication(publication_id: PublicationDocument["_id"]): Promise<PublicationDocument | null> {
        return Publication.findOne({_id: publication_id})
            .populate({
                path: 'user',
                populate: [{path: 'profile'}]
            })
            .populate({
                path: 'layouts',

                populate: [
                    {path: "source"},
                    {path: "sources"}
                ]
            })
            .exec()
    }

    async updateLikeState(publication_id: PublicationDocument["id"], user_id: UserDocument["_id"]): Promise<void> {
        const publication = await Publication.findOne({
            _id: publication_id
        });

        if (publication !== null) {
            if (publication.likes.includes(user_id)) {
                const index = publication.likes.indexOf(publication_id);
                publication.likes.splice(index, 1);
                await publication.save();
            } else {
                publication.likes.push(user_id);
                await publication.save();
            }
        }
    }

    getPublicationsByUserId(user_id: UserDocument["_id"]): Promise<PublicationDocument[]> {
        return Publication.find({user: user_id})
            .populate({
                path: 'user',
                populate: [{path: 'profile'}]
            }).populate({
                path: 'layouts',
                populate: [{path: "source"}, {path: "sources"}]
            }).exec();
    }

}