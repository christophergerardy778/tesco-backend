import {UserDocument} from "../../../infrastructure/orm/User";
import {ProfileDocument} from "../../../infrastructure/orm/Profile";
import {ISkillResult} from "../../entity/profile/ISkillResult";

export interface IChartRepository {
    getProfileIds(): Promise<ProfileDocument["_id"]>;
    getGraduateAlumnus(profile_ids: ProfileDocument["_id"][]): Promise<ProfileDocument[]>;
    getAlumnus() : Promise<UserDocument[]>;
    getGraduatesWithExperience() : Promise<ProfileDocument[]>;
    getGraduatesWithLinkedinProfile(): Promise<ProfileDocument[]>;
    getPopularSkills() : Promise<ISkillResult[]>;
}