import {IChartRepository} from "./IChartRepository";
import User, {UserDocument} from "../../../infrastructure/orm/User";
import Profile, {ProfileDocument} from "../../../infrastructure/orm/Profile";
import Skill from "../../../infrastructure/orm/Skill";
import {ISkillResult} from "../../entity/profile/ISkillResult";
import mongoose from "mongoose";

export class ChartRepository implements IChartRepository {
    getGraduateAlumnus(profile_ids: ProfileDocument["_id"][]): Promise<ProfileDocument[]> {
        return Profile.find({
            has_job: true,
            _id: {$in: profile_ids}
        }).exec()
    }

    getProfileIds(): Promise<ProfileDocument["_id"]> {
        return User.find({
            graduate: true
        }).exec();
    }

    getAlumnus(): Promise<UserDocument[]> {
        return User.find({
            graduate: false
        }).exec();
    }

    getGraduatesWithExperience(): Promise<ProfileDocument[]> {
        return Profile.find({
            experience: {$exists: true, $not: {$size: 0}}
        }).exec()
    }

    getGraduatesWithLinkedinProfile(): Promise<ProfileDocument[]> {
        return Profile.find({
            linkedin: {$ne: ""}
        }).exec()
    }

    async getPopularSkills(): Promise<ISkillResult[]> {
        const result: ISkillResult[] = [];

        const skillsQuery = await Skill.find()
            .select("name")
            .exec();

        for (const item of skillsQuery) {

            const profileQuery = await Profile.find({
                skills: {
                    $elemMatch: { $eq: new mongoose.Types.ObjectId(item._id) }
                }
            }).exec()

            result.push({
                technology: item.name,
                usages: profileQuery.length
            })
        }

        return result;
    }
}