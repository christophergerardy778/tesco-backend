import {IProfileRepository} from "./IProfileRepository";
import Profile, {ProfileDocument} from "../../../infrastructure/orm/Profile";
import {GetProfilePort} from "../../../infrastructure/uses_cases/profile/port/GetProfilePort";
import User, {UserDocument} from "../../../infrastructure/orm/User";
import {UpdateProfilePort} from "../../../infrastructure/uses_cases/profile/port/UpdateProfilePort";
import {UpdateProfilePhotoPort} from "../../../infrastructure/uses_cases/profile/port/UpdateProfilePhotoPort";
import {UpdateSkillsPort} from "../../../infrastructure/uses_cases/profile/port/UpdateSkillsPort";
import {AddExperiencePort} from "../../../infrastructure/uses_cases/profile/port/AddExperiencePort";
import Experience from "../../../infrastructure/orm/Experience";
import {DeleteExperiencePort} from "../../../infrastructure/uses_cases/profile/port/DeleteExperiencePort";
import {UpdateExperiencePort} from "../../../infrastructure/uses_cases/profile/port/UpdateExperiencePort";

export default class ProfileRepository implements IProfileRepository {

    async updateProfilePhoto(port: UpdateProfilePhotoPort): Promise<void> {
        await Profile.updateOne({
            _id: port.id
        }, {
            photo: port.photo.filename
        })
    }

    createEmptyProfile(): Promise<ProfileDocument> {
        return Profile.create({
            web_url: "",
            facebook: "",
            linkedin: "",
            job: "",
            photo: "",
            company: "",
            skills: [],
            experience: [],
            description: "",
        });
    }

    getProfileById(port: GetProfilePort): Promise<UserDocument | null> {
        return User.findOne({
            _id: port.user_id
        })
            .select("active enrollment roll graduate dateOfAdmission egressDate _id name lastname email")
            .populate({
                path: 'profile',
                populate: [
                    {path: 'experience'},
                    {path: 'skills'}
                ],
            }).exec();
    }

    async updateProfile(port: UpdateProfilePort) {
        await Profile.updateOne({
            _id: port.id
        }, port.profile);
    }

    async updateSkills(port: UpdateSkillsPort): Promise<void> {
        await Profile.updateOne({
            _id: port.id
        }, {
            skills: port.skills
        }).exec();
    }

    async addExperience(port: AddExperiencePort): Promise<void> {
        const experience = await Experience.create(port.experience);

        await Profile.updateOne({
            _id: port.id
        }, {
            $push: {experience: experience._id}
        }).exec()
    }

    async deleteExperience(port: DeleteExperiencePort): Promise<void> {

        await Profile.updateOne({
            _id: port.profile_id
        }, {
            $pull: {
                experience: port.id
            }
        })

        await Experience.deleteOne({_id: port.id});
    }

    async updateExperience(port: UpdateExperiencePort): Promise<void> {
        await Experience.updateOne({_id: port.id}, port.experience).exec();
    }
}