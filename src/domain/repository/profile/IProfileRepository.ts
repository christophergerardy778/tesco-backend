import {ProfileDocument} from "../../../infrastructure/orm/Profile";
import {GetProfilePort} from "../../../infrastructure/uses_cases/profile/port/GetProfilePort";
import {UserDocument} from "../../../infrastructure/orm/User";
import {UpdateProfilePort} from "../../../infrastructure/uses_cases/profile/port/UpdateProfilePort";
import {UpdateProfilePhotoPort} from "../../../infrastructure/uses_cases/profile/port/UpdateProfilePhotoPort";
import {UpdateSkillsPort} from "../../../infrastructure/uses_cases/profile/port/UpdateSkillsPort";
import {AddExperiencePort} from "../../../infrastructure/uses_cases/profile/port/AddExperiencePort";
import {DeleteExperiencePort} from "../../../infrastructure/uses_cases/profile/port/DeleteExperiencePort";
import {UpdateExperiencePort} from "../../../infrastructure/uses_cases/profile/port/UpdateExperiencePort";

export interface IProfileRepository {
    createEmptyProfile() : Promise<ProfileDocument>;
    getProfileById(port: GetProfilePort) : Promise<UserDocument | null>;

    updateProfile(port: UpdateProfilePort): Promise<void>;
    updateProfilePhoto(port: UpdateProfilePhotoPort) : Promise<void>;
    updateSkills(port: UpdateSkillsPort): Promise<void>;
    updateExperience(port: UpdateExperiencePort): Promise<void>;

    addExperience(port: AddExperiencePort): Promise<void>;
    deleteExperience(port: DeleteExperiencePort): Promise<void>;
}