import {ISkillRepository} from "./ISkillRepository";
import Skill, {SkillDocument} from "../../../infrastructure/orm/Skill";

export default class SkillRepository implements ISkillRepository {
    createSkill(name: string, color: string): Promise<SkillDocument> {
        return Skill.create({
            name,
            color
        });
    }

    getAllSkills(): Promise<SkillDocument[]> {
        return Skill.find().exec();
    }

    async updateSkill(skill_id: string, name: string, color: string): Promise<void> {
        await Skill.updateOne({
            _id: skill_id
        }, {
            name,
            color
        }).exec()
    }

    async deleteSkill(skill_id: string): Promise<void> {
        await Skill.deleteOne({ _id: skill_id })
    }
}