import {SkillDocument} from "../../../infrastructure/orm/Skill";

export interface ISkillRepository {
    createSkill(name: string, color: string) : Promise<SkillDocument>;
    getAllSkills() : Promise<SkillDocument[]>;
    updateSkill(skill_id: string, name: string, color: string) : Promise<void>;
    deleteSkill(skill_id: string) : Promise<void>;
}