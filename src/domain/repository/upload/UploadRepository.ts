import {IUploadRepository} from "./IUploadRepository";
import {IFileTypes} from "../../entity/files/IFile";
import File, {FileDocument} from "../../../infrastructure/orm/File";

export default class UploadRepository implements IUploadRepository {
    createFile(name: string, type: IFileTypes): Promise<FileDocument> {
        return File.create({
            name,
            type
        });
    }

    async findMany(idArray: string[]): Promise<FileDocument[]> {
        return File.find()
            .where("_id")
            .in(idArray)
            .exec();
    }

    async deleteMany(idArray: string[]): Promise<void> {
        await File.deleteMany({
            _id: {
                $in: idArray
            }
        });
    }
}