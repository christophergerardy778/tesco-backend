import {FileDocument} from "../../../infrastructure/orm/File";
import {IFileTypes} from "../../entity/files/IFile";

export interface IUploadRepository {
    createFile(name: string, type: IFileTypes) : Promise<FileDocument>;
    findMany(idArray: string[]) : Promise<FileDocument[]>;
    deleteMany(idArray: string[]) : Promise<void>;
}