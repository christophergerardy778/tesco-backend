import {NoticeDocument} from "../../../infrastructure/orm/Notice";

export interface INoticeRepository {
    createNotice(
        title: string,
        description: string,
        user: string,
        layouts: string[],
        cover?: string
    ) : Promise<NoticeDocument>;

    getPaginateNotices(
        page: number
    ) : Promise<NoticeDocument[]>;

    getNoticeById(
        notice_id: string
    ) : Promise<NoticeDocument | null>;

    deleteNoticeById(
        notice_id: string
    ) : Promise<void>;
}