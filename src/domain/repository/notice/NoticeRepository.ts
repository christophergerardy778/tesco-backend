import {INoticeRepository} from "./INoticeRepository";
import Notice, {NoticeDocument} from "../../../infrastructure/orm/Notice";

export default class NoticeRepository implements INoticeRepository {
    async createNotice (
        title: string,
        description: string,
        user: string,
        layouts: string[],
        cover: string
    ) : Promise<NoticeDocument> {
        return Notice.create({
            user,
            title,
            description,
            cover,
            layouts
        });
    }

    getPaginateNotices(page: number): Promise<NoticeDocument[]> {
        return Notice.find()
            .populate({
                path: "layouts",

                populate: [
                    { path: "source" },
                    { path: "sources" }
                ]
            })
            .skip(page * 10)
            .limit(10)
            .sort({ createdAt: "desc" })
            .exec()
    }

    getNoticeById(notice_id: string): Promise<NoticeDocument | null> {
        return Notice.findOne({
            _id: notice_id
        })
            .populate({
                path: "layouts",

                populate: [
                    { path: "source" },
                    { path: "sources" }
                ]
            })
            .exec();
    }

    async deleteNoticeById(notice_id: string) : Promise<void> {
        await Notice.deleteOne({
            _id: notice_id
        }).exec();
    }
}