import {UserDocument} from "../../../infrastructure/orm/User";
import {FilterQuery} from "mongoose";
import {IUser, Role} from "../../entity/auth/IUser";
import {ProfileDocument} from "../../../infrastructure/orm/Profile";

export interface IAuthRepository {
    createUser(user: UserDocument, profile: ProfileDocument) : Promise<UserDocument>;

    getUserByEmail(email: string) : Promise<UserDocument | null>;
    getCleanUser(email: string) : Promise<UserDocument | null>;
    getUserByEmailAndRole(email: string, role: Role[]) : Promise<UserDocument | null>;
    getUserByEmailAndAllowRoles(email: string) : Promise<UserDocument | null>;
    getOneUser(filterQuery: FilterQuery<any>) : Promise<UserDocument | null>;

    updateStatus(_id: string, active: boolean): Promise<void>
    updatePassword(email: string, hash: string) : Promise<void>
    updateAttemptInformation(
        user_id: string,
        graduate: boolean,
        egressDate: Date,
        enrollment: string
    ) : Promise<void>;

    updateUser(id: string, user: Partial<IUser>) : Promise<void>;
    deleteUser() : string;
    getAllUsers() : string;

    deleteOneUser(filterQuery: FilterQuery<any>) : string;
    getUsersByRoleModerator(): Promise<UserDocument[]>;
}