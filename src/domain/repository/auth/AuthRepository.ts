import User, {UserDocument} from "../../../infrastructure/orm/User";
import {IAuthRepository} from "./IAuthRepository";

import {FilterQuery} from "mongoose";
import {IUser, Role} from "../../entity/auth/IUser";
import {ProfileDocument} from "../../../infrastructure/orm/Profile";

export default class AuthRepository implements IAuthRepository {

    async updateStatus(_id: string, active: boolean): Promise<void> {
        await User.updateOne({
            _id
        }, {
            active: active
        })
    }

    getUserByEmail(email: string): Promise<UserDocument | null> {
        return User.findOne({
            email: email
        }).exec();
    }

    async createUser(user: UserDocument, profile: ProfileDocument): Promise<UserDocument> {
        return User.create({
            ...user,
            profile: profile._id
        });
    }

    deleteUser(): string {
        return "";
    }

    getAllUsers(): string {
        return "";
    }

    getOneUser(filterQuery: FilterQuery<any>): Promise<UserDocument | null> {
        return User.findOne(filterQuery).exec();
    }

    deleteOneUser(filterQuery: FilterQuery<any>): string {
        return "";
    }

    getUserByEmailAndRole(email: string, role: Role[]): Promise<UserDocument | null> {
        return User.findOne({
            email
        }).exec();
    }

    getCleanUser(email: string): Promise<UserDocument | null> {
        return User.findOne({
            email: email
        }).select("name lastname graduate enrollment active dateOfAdmission egressDate email")
            .exec();
    }

    async updatePassword(email: string, hash: string): Promise<void> {
        await User.updateOne({
            email: email
        }, {
            password: hash
        });
    }

    async getUserByEmailAndAllowRoles(email: string): Promise<UserDocument | null> {
        return User.findOne({
            email: email,
            roll: {
                $in: [
                    Role.ADMIN,
                    Role.MODERATOR
                ]
            }
        })
    }

    async updateAttemptInformation(
        user_id: string,
        graduate: boolean,
        egressDate: Date,
        enrollment: string
    ): Promise<void> {
        await User.updateOne({
            _id: user_id
        }, {
            graduate,
            egressDate,
            enrollment
        }).exec()
    }

    async updateUser(id: string, user: Partial<IUser>): Promise<void> {
        await User.updateOne({
            _id: id
        }, user);
    }

    async getUsersByRoleModerator(): Promise<UserDocument[]> {
        return User.find({
            roll: Role.MODERATOR
        }).exec();
    }
}