import {LayoutDocument} from "../../../infrastructure/orm/Layout";
import {ILayout} from "../../entity/layout/ILayout";

export interface ILayoutRepository {
    createLayouts(layouts: ILayout[]) : Promise<LayoutDocument[]>
}