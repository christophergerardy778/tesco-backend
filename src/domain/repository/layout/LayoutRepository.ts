import {ILayoutRepository} from "./ILayoutRepository";
import {ILayout} from "../../entity/layout/ILayout";
import Layout, {LayoutDocument} from "../../../infrastructure/orm/Layout";

export default class LayoutRepository implements ILayoutRepository{
    async createLayouts(layouts: ILayout[]): Promise<LayoutDocument[]> {
        return Layout.insertMany(layouts);
    }

}