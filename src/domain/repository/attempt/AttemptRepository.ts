import {IAttemptRepository} from "./IAttemptRepository";
import Attempt, {AttemptDocument} from "../../../infrastructure/orm/Attempt";
import {IAttempt, STATUS_ATTEMPT} from "../../entity/attempt/IAttempt";

export default class AttemptRepository implements IAttemptRepository {
    createAttempt(attempt: IAttempt): Promise<AttemptDocument> {
        return Attempt.create(attempt);
    }

    findAttemptByIdWithUser(attemptId: string, userId: string): Promise<AttemptDocument | null> {
        return Attempt.findOne({
            _id: attemptId,
            user: userId
        }).exec();
    }

    async updateState(attemptId: string, state: STATUS_ATTEMPT): Promise<void> {
        await Attempt.updateOne({
            _id: attemptId
        }, {
            status: state
        }).exec();
    }

    async deleteAttempt(userId: string, attemptId: string): Promise<void> {
        await Attempt.deleteOne({
            _id: attemptId,
            user: userId
        }).exec();
    }

    async getAttemptsByStatus(status: STATUS_ATTEMPT): Promise<AttemptDocument[]> {
        return Attempt.find({
            status: status
        }).populate("user", [
            "_id",
            "name",
            "email",
            "active",
            "lastname",
            "graduate",
            "egressDate",
            "updatedAt",
            "createdAt",
            "dateOfAdmission"
        ]).exec();
    }

    async getPaginateAttempts(perPage: number, status: STATUS_ATTEMPT): Promise<AttemptDocument[]> {
        return Attempt.find({ status })
            .skip(perPage * 10)
            .limit(10)
            .sort({ createdAt: "desc" })
            .populate("user", [
                "_id",
                "name",
                "email",
                "active",
                "lastname",
                "graduate",
                "egressDate",
                "updatedAt",
                "enrollment",
                "createdAt",
                "dateOfAdmission"
            ])
            .exec()
    }

    getAttemptsByUserId(user_id: string): Promise<AttemptDocument[]> {
        return Attempt.find({
            user: user_id
        })
            .sort({ createdAt: "desc" })
            .exec();
    }
}