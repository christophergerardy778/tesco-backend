import {AttemptDocument} from "../../../infrastructure/orm/Attempt";
import {IAttempt, STATUS_ATTEMPT} from "../../entity/attempt/IAttempt";

export interface IAttemptRepository {
    createAttempt(attempt: IAttempt) : Promise<AttemptDocument>;
    findAttemptByIdWithUser(attemptId: string, userId: string) : Promise<AttemptDocument | null>;
    updateState(attemptId: string, state: STATUS_ATTEMPT) : Promise<void>;
    deleteAttempt(userId: string, attemptId: string) : Promise<void>;
    getAttemptsByStatus(status: STATUS_ATTEMPT) : Promise<AttemptDocument[]>;
    getAttemptsByUserId(user_id: string) : Promise<AttemptDocument[]>;
    getPaginateAttempts(perPage: number, status: STATUS_ATTEMPT) : Promise<AttemptDocument[]>
}